var workouts;

function readWorkouts( callback ) {
  var fields = [];
  workouts = {};
  d3.xml("workouts.xml", function(data) {
    fields = [].map.call(data.querySelectorAll("workout_file"), function(workout_file) {
      var name = workout_file.querySelector("name").textContent;
      workouts[name] = workout_file;
      return name;
    });

    renderSelect("#workouts", fields, "")
    d3.selectAll('#workouts').on('change', function() {
      workoutName = this.value;
      intervals = matchTracks();
      intervalTable(intervals);
      startMusic(intervals);
    });

  })
}

function matchTracks() {
  intervals = [];

  var workout = workouts[workoutName];
  var preintervals = [].map.call(workout.querySelectorAll("Warmup, IntervalsT, SteadyState, Ramp, Cooldown"), function(warmup) {
    return {
      repeat: Number(warmup.getAttribute("Repeat")),
      duration: Number(warmup.getAttribute("Duration")),
      power: Number(warmup.getAttribute("Power")),
      onDuration: Number(warmup.getAttribute("OnDuration")),
      offDuration: Number(warmup.getAttribute("OffDuration")),
      powerLow: Number(warmup.getAttribute("PowerLow")),
      powerHigh: Number(warmup.getAttribute("powerHigh")),
      cadenceResting: Number(warmup.getAttribute("CadenceResting")),
      cadence: Number(warmup.getAttribute("Cadence")),
      onPower: Number(warmup.getAttribute("OnPower")),
      offPower: Number(warmup.getAttribute("OffPower")),
    };
  });
  preintervals.forEach(function(d) {
    var repeat = Math.max(1, d.repeat);
    if( d.onDuration > 0 )
      repeat *= 2;
    for (var i = 0; i < repeat; i++) {
      var interval = {};

      interval.duration = getDuration(d, i);
      interval.power = getPower(d, i);
      interval.cadence = getCadence(d, i);

      intervals.push(interval);
    }
  })
  powerScale.range(d3.extent(intervals, function(i) {
    return i.power;
  }));

  intervals.forEach(function(interval) {
    interval.tracks = matchingTempo(interval, trackList);
    if( interval.tracks.length == 0){
      alert("No track with tempo for "+interval.cadence+" interval cadence, select another playlist.")
    }
  })
  var cadenceGroups = {};
  intervals.forEach(function(interval) {
    let cadence = interval.cadence;
    if (!cadenceGroups[cadence]) {
      cadenceGroups[cadence] = [];
    }
    cadenceGroups[cadence].push(interval);
  })

  intervals.forEach(function(interval) {
    interval.track = bestTrack(interval, cadenceGroups[interval.cadence]);
    interval.track.used = true;
  })

  return intervals;
}

function getDuration(d, i) {
  let duration = d.duration;
  if (d.onDuration > 0) {
    if (i % 2 == 0) {
      return d.onDuration;
    } else {
      return d.offDuration;
    }
  }
  return duration;
}

function getPower(d, i) {
  if (d.power > 0) return d.power;
  if (d.onPower > 0) {
    if (i % 2 == 0) {
      return d.onPower;
    } else {
      return d.offPower;
    }
  }
  return 0.5;
}

function getCadence(d, i) {

  let cadence = d.cadence;
  if (cadence == 0) cadence = 90;
  if (d.cadenceResting > 0) {
    if (i % 2 == 0) {
      return d.cadence;
    } else {
      return d.cadenceResting;
    }
  }
  return cadence;
}
