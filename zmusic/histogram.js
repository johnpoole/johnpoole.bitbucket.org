var Histogram = (function () {
  // set the dimensions and margins of the graph
  var margin = {top: 5, right: 10, bottom: 20, left: 30},
      width = 290 - margin.left - margin.right,
      height = 200 - margin.top - margin.bottom;

  // set the ranges
  var x = d3.scaleLinear()
            .domain([ 0, 1])
            .rangeRound([0, width]);
  var y = d3.scaleLinear()
            .range([height, 0]);

    return {
               draw_histogram : function ( trackdata, category) {
                 d3.select("#"+category+"-chart").select("svg").remove()
                 var data = Object.values(trackdata).map(function(d){
                   return d.features[category];
                 })
                  var svg = d3.select("#"+category+"-chart").append("svg")
                      .attr("width", width + margin.left + margin.right)
                      .attr("height", height + margin.top + margin.bottom)
                    .append("g")
                      .attr("transform",
                            "translate(" + margin.left + "," + margin.top + ")");

                  x.domain( d3.extent(data, function(d){
                    var val = d;
                    return d;
                  }));
                  var histogram = d3.histogram()
                      .value(function(d) {
                        return d;
                      })
                      .domain(x.domain())
                      .thresholds(x.ticks(30));
                  // format the data
                  data.forEach(function(d) {
                  });

                  // group the data for the bars
                  var bins = histogram(data);

                  // Scale the range of the data in the y domain
                  y.domain([0, d3.max(bins, function(d) { return d.length; })]);

                  // append the bar rectangles to the svg element
                  svg.selectAll("rect")
                      .data(bins)
                    .enter().append("rect")
                      .attr("class", "bar")
                      .attr("x", 1)
                      .attr("transform", function(d) {
                		  return "translate(" + x(d.x0) + "," + y(d.length) + ")"; })
                      .attr("width", function(d) { return x(d.x1) - x(d.x0) -1 ; })
                      .attr("height", function(d) { return height - y(d.length); });

                  // add the x Axis
                  svg.append("g")
                      .attr("transform", "translate(0," + height + ")")
                      .call(d3.axisBottom(x));

                  // add the y Axis
                  svg.append("g")
                      .call(d3.axisLeft(y));
                }
              }
})();
