var playlist_id = '1Bd0IfvDgnbAyY44cUEOhz';
const attribute = "energy";
var currentId = -1;

// Get the hash of the url
const hash = window.location.hash
  .substring(1)
  .split('&')
  .reduce(function(initial, item) {
    if (item) {
      var parts = item.split('=');
      initial[parts[0]] = decodeURIComponent(parts[1]);
    }
    return initial;
  }, {});
window.location.hash = '';

// Set token
let _token = hash.access_token;

const authEndpoint = 'https://accounts.spotify.com/authorize';

// Replace with your app's client ID, redirect URI and desired scopes
const clientId = '4a0eebaf6e1e49f58665a8eb4e3744a6';
const redirectUri = 'http://johnpoole.bitbucket.io/zmusic';
const scopes = [
  'streaming',
  'user-read-private',
  'user-modify-playback-state'
];
var workoutName;

var trackIds;
var trackList = {};
var intervals = [];
var deviceID;
//d3
var powerScale = d3.scaleLinear();

// If there is no token, redirect to Spotify authorization
if (!_token) {
  window.location = `${authEndpoint}?client_id=${clientId}&redirect_uri=${redirectUri}&scope=${scopes.join('%20')}&response_type=token&show_dialog=true`;
}

// Set up the Web Playback SDK
window.onSpotifyPlayerAPIReady = () => {
  var playLists = getPlayLists();

  intervals = readWorkouts();

}

// Play a specified track on the Web Playback SDK's device ID
function play(device_id, spotify_uri) {
//  console.log("spotify_uri: " + spotify_uri);
    let index = currentId;
    d3.request("https://api.spotify.com/v1/me/player/play?device_id=" + device_id)
    .header("Content-Type", "application/json")
    .header("Authorization", 'Bearer ' + _token)
    .send("PUT",
    JSON.stringify({ uris: [spotify_uri], position_ms:5000 }), function( d ){
      var row = d3.select(".ID"+(index));
      row.classed("table-success", true);
      $(".ID"+(index)).get(0).scrollIntoView();
      console.log( d );
    })
}


function getPlayListInformation(playlist_id, offset) {
  d3.json( "https://api.spotify.com/v1/playlists/" + playlist_id + "/tracks?offset="+offset)
  .header("Authorization", 'Bearer ' + _token)
  .get(function( json ){
      console.log(json)
      trackIds = "ids=";
      json.items.forEach(function(d) {
        trackIds += d.track.id + ",";
        trackList[d.track.id] = d.track;
      })
      getSpotifyAudioFeatures(trackIds, trackList);
      if( json.offset+json.limit < json.total){
          getPlayListInformation(playlist_id, offset+1000);
      }
    });
}
//
function getPlayLists() {
  var playlists = [];

  d3.json( "https://api.spotify.com/v1/me/playlists").header("Authorization", 'Bearer ' + _token)
  .get(function( json ){
      json.items.forEach(function(d) {
        playlists.push( d );
      })

      renderPSelect("#playlists", playlists, "")
      d3.selectAll('#playlists').on('change', function(){
        playlist_id = this.value
        getPlayListInformation(playlist_id, 0)
      });
    });
    return playlists;
}


function getSpotifyAudioFeatures(trackIds){

  d3.json(  "https://api.spotify.com/v1/audio-features/?" + trackIds)
  .header("Authorization", 'Bearer ' + _token)
  .get(function( data ){
       data.audio_features.forEach( function(af){
         if( af )
          trackList[af.id].features = af;
      });

      Histogram.draw_histogram(trackList, "tempo");
      Histogram.draw_histogram(trackList, "energy");
      Histogram.draw_histogram(trackList, "valence");
  });
}



function startMusic(intervals) {

  const player = new Spotify.Player({
    name: 'Web Playback SDK Template',
    getOAuthToken: cb => {
      cb(_token);
    }
  });

  // Error handling
  player.on('initialization_error', e => console.error(e));
  player.on('authentication_error', e => console.error(e));
  player.on('account_error', e => console.error(e));
  player.on('playback_error', e => console.error(e));

  // Playback status updates
  player.on('player_state_changed', state => {
    console.log(state)
    if( typeof(state) != "undefined" && state != null ){
      $('#current-track').attr('src', state.track_window.current_track.album.images[0].url);
      $('#current-track-name').text(state.track_window.current_track.name);
    }
  });

  // Ready
  player.on('ready', data => {
    console.log('Ready with Device ID', data.device_id);
    deviceID = data.device_id;
    // Play a track using our new device ID
  });

  // Connect to the player!
  player.connect();
}
//timeout methods
var interval_timeout;
var extra_timeout; //if the interval is longer than the song
function playNext() {
  clearTimeout(interval_timeout);
  clearTimeout(extra_timeout);
  currentId++;

  var track_id = intervals[currentId].track.uri;
  play(deviceID, track_id);
  var duration_diff = intervals[currentId].duration - intervals[currentId].track.features.duration_ms/1000;
  if( duration_diff > 0 ){
    let timeout_time = intervals[currentId].track.features.duration_ms;
    extra_timeout = setTimeout( playExtra, timeout_time  );
  }
  if (currentId <= intervals.length)
    interval_timeout = setTimeout(playNext, intervals[currentId].duration * 1000);
}
//keep playing the song until it's replaced by the next interval trqck
function playExtra() {
  clearTimeout(extra_timeout);
  play(deviceID, intervals[currentId].track.uri);
  extra_timeout = setTimeout(playExtra, intervals[currentId].track.features.duration_ms);
}

function pauseMusic(){
  clearTimeout(interval_timeout);
  clearTimeout(extra_timeout);

  d3.request("https://api.spotify.com/v1/me/player/pause")
  .header("Content-Type", "application/json")
  .header("Authorization", 'Bearer ' + _token)
  .send("PUT", function( d ){
    console.log( d );
  })
}

function matchingTempo( interval, trackList){
  tracks = [];
  Object.values(trackList).forEach(function(t) {
      if( t != null ){
        let diff = Math.abs(t.features.tempo  - interval.cadence)
        diff = Math.min( diff, Math.abs(t.features.tempo / 2 - interval.cadence))
        if( diff < 2 ){
          tracks.push(t);
        }
      }
  })
  return tracks;
}

//TODO: don't re-use tracks
//need to find a formula for different power levels matching to music qualities
function bestTrack( interval, cadenceGroup){
  var best = interval.tracks[0];
  var match = 1000;
  powerScale.domain( d3.extent( interval.tracks, function(t){
    return t.features[attribute];
  }))
  powerScale.range(d3.extent(Object.values(cadenceGroup), function(i){
    return i.power;
  }));
  let attributeValue = powerScale.invert( interval.power);

  interval.tracks.forEach(function(t) {
    if( t != null && t != cadenceGroup.last){
      let diff = Math.abs( t.features[attribute] - attributeValue);

       if (diff < match) {
          best = t;
          match = diff;
      }
    }
  })
  cadenceGroup.last = best;
  return best;
}


function   renderSelect(id, fields, value){
  d3.select(id).selectAll("option").data(fields).enter().append("option")
    .text(function(d){return d;})
  d3.select(id).property("selectedIndex", fields.indexOf(value));
}
function   renderPSelect(id, fields, value){
  d3.select(id).selectAll("option").data(fields).enter().append("option")
  .attr("value",function(d){
    return d.id;
  })
    .text(function(d){
      return d.name;
    })
  d3.select(id).property("selectedIndex", fields.indexOf(value));
}


function intervalTable(data){
  var table = d3.select("#table").append("table").attr("class","table");
        var header = table.append("thead").append("tr");
        header
                .selectAll("th")
                .data(["Time(s)", "Power", "Cadence", "Options","Tempo", "Track"])
                .enter()
                .append("th")
                .text(function(d) { return d; });
        var tablebody = table.append("tbody");
        rows = tablebody
                .selectAll("tr")
                .data(data)
                .enter()
                .append("tr")
                .attr("class",function(d, i){
                  return "ID"+i;
                });
        // We built the rows using the nested array - now each row has its own array.
        cells = rows.selectAll("td")
            // each row has data associated; we get it and enter it for the cells.
                .data(function(d) {
                    return [d.duration, d.power, d.cadence, d.tracks.length, d.track.features.tempo, d.track.name];
                })
                .enter()
                .append("td")
                .text(function(d) {
                    return d;
                });
}
