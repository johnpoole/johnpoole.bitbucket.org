function FrontierLegend() {
	this.width = 750;
	this.height = 75;
	this.textWidth = 60;
  this.statusColour = [{
    "type": "Site",
    "statuses": {
      "None": "#000000",
      "Design": "#C51AF0",
      "Scouted": "#66FFFF",
      "Entered": "#FFFF00",
      "Built": "#00B050"
    }
  }, {
    "type": "Auger",
    "statuses": {
      "None": "#00B050",
      "Spud": "#F79646",
      "TD": "#F79646",
      "Install": "#F79646",
      "Rig Released": "#FF0000",
      "As Built": "#FFFFFF",
      "Rolled Back": "#808080"
    }
  }, {
    "type": "Sonic",
    "statuses": {
      "None": "#00B050",
      "Spud": "#F79646",
      "TD": "#F79646",
      "Install": "#F79646",
      "Rig Released": "#FF0000",
      "As Built": "#FFFFFF",
      "Rolled Back": "#808080"
    }
  }, {
    "type": "Resource",
    "statuses": {
      "None": "#00B050",
      "Spud": "#F79646",
      "Casing Set": "#F79646",
      "TD": "#F79646",
      "Rig Released": "#FF0000",
      "Wireline Logged": "#7030A0",
      "Abn-Start": "#8DB4E2",
      "Abn-Rig Rel": "#1F497D",
      "As Built": "#FFFFFF",
      "Rolled Back": "#808080"
    }
  }];
}

FrontierLegend.prototype.addTo = function(div) {
  var legendModal = d3.select("#" + div);
  var svg = legendModal.select(".modal-body")
    .append("svg").attr("id", 'legend')
    .attr("width", this.width).attr("height", this.height);
  var g = svg.append("g").attr("class", "key").attr(
    "transform", "translate(65,5)");

 var count = Math.max.apply(Math, this.statusColour.map(function(s) { return Object.values(s.statuses).length; }))

  var legBlockWidth = this.width/(count +1),
    legBlockHeight = this.height/(this.statusColour.length + 1);
  //each row of legend
  var statusTypes = g.selectAll("g").data(this.statusColour)
    .enter().append("g").attr(
      "transform",
      function(d, i) {
        return "translate(5," + i *
          legBlockHeight + ")";
      });
  statusTypes.selectAll("rect").data(function(d, i) {
      var statusArray = [];
      for (var key in d.statuses) {
        var value = d.statuses[key];
        statusArray.push(i + "," + value);
      }
      return statusArray;
    }).enter().append("rect")
    .attr("x", function(d, i) {
      return i * legBlockWidth;
    })
    .attr("y", 0).attr("width", legBlockWidth - 2)
    .attr("height", legBlockHeight - 2)
    .attr("fill",
      function(d, i) {
        if (d.split(",")[0] > 0)
          return d.split(",")[1];
        return "#ffffff";
      })
    .attr("stroke",
      function(d) {
        return d.split(",")[1];
      });

  statusTypes.selectAll("text").data(function(d) {
      var statusArray = [];
      for (var key in d.statuses) {
        statusArray.push(key);
      }
      return statusArray;
    }).enter().append("text").attr("x", function(d, i) {
      return i * legBlockWidth;
    })
    .attr("y", 10)
    //.style("fill", function(d, i) {
    //	console.log( "invert "+d);
    //	return invertColor(d);
    //})
    .text(function(d) {
      return d.substring(0, 11);
    });

  var gshapes = svg.append("g").attr("class", "shapes");

 this.statusColour.forEach( function(lease, i){
	 gshapes.append("text").attr("x", 0).attr("transform",
     "translate(5," + legBlockHeight*(1+i) + ")").text(lease.type)
			 })

  //map symbols
	new FrontierShapes().drawAug(gshapes, this.textWidth, legBlockHeight * 2.5 + 5, "Black","None");
	new FrontierShapes().drawSonic(gshapes, this.textWidth, legBlockHeight * 1.5 + 5, "Black","None");
	new FrontierShapes().drawCH(gshapes, this.textWidth, legBlockHeight * 3.5 +5, "Black","None") ;
};
