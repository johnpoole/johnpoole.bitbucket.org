var OPACITY = 0.3;

var map, featureList, boroughSearch = [], wellsiteSearch = [], siteData=[];
var statuses = [
  "None",
  "As-Built",
  "Design",
  "Scouted",
  "Entered",
  "Built"
];
var lease_types = ["SONIC","CH","AUG"];
var leaseSymbols = d3.scaleOrdinal()
.domain(lease_types)
.range([d3.symbolCircle,d3.symbolSquare,d3.symbolTriangle]);
var statusColour = d3.scaleOrdinal()
  .domain(statuses)
  .range(d3.schemeSet1  );

$(window).resize(function() {
  sizeLayerControl();
});

$(document).on("click", ".feature-row", function(e) {
  $(document).off("mouseout", ".feature-row", clearHighlight);
  sidebarClick(parseInt($(this).attr("id"), 10));
});

if ( !("ontouchstart" in window) ) {
  $(document).on("mouseover", ".feature-row", function(e) {
    highlight.clearLayers().addLayer(L.circleMarker([$(this).attr("lat"), $(this).attr("lng")], highlightStyle));
  });
}

$(document).on("mouseout", ".feature-row", clearHighlight);

$("#about-btn").click(function() {
  $("#aboutModal").modal("show");
  $(".navbar-collapse.in").collapse("hide");
  return false;
});

$("#full-extent-btn").click(function() {
  map.fitBounds(wellsites.getBounds());
  $(".navbar-collapse.in").collapse("hide");
  return false;
});

$("#legend-btn").click(function() {
  $("#legendModal").modal("show");
  $(".navbar-collapse.in").collapse("hide");
  return false;
});

$("#login-btn").click(function() {
  $("#loginModal").modal("show");
  $(".navbar-collapse.in").collapse("hide");
  return false;
});

$("#list-btn").click(function() {
  animateSidebar();
  return false;
});

$("#nav-btn").click(function() {
  $(".navbar-collapse").collapse("toggle");
  return false;
});

$("#sidebar-toggle-btn").click(function() {
  animateSidebar();
  return false;
});

$("#sidebar-hide-btn").click(function() {
  animateSidebar();
  return false;
});

function animateSidebar() {
  $("#sidebar").animate({
    width: "toggle"
  }, 350, function() {
    map.invalidateSize();
  });
}

function sizeLayerControl() {
  $(".leaflet-control-layers").css("max-height", $("#map").height() - 50);
}

function clearHighlight() {
  highlight.clearLayers();
}

function sidebarClick(id) {
  var layer = markerClusters.getLayer(id);
  map.setView([layer.getLatLng().lat, layer.getLatLng().lng], 17);
  layer.fire("click");
  /* Hide sidebar and go to the map on small screens */
  if (document.body.clientWidth <= 767) {
    $("#sidebar").hide();
    map.invalidateSize();
  }
}

function syncSidebar() {
  /* Empty sidebar features */
  $("#feature-list tbody").empty();

  /* Loop through wellsites layer and add only features which are in the map bounds */
  wellsites.eachLayer(function (layer) {
    if (map.hasLayer(wellsiteLayer)) {
      if (map.getBounds().contains(layer.getLatLng())) {
        $("#feature-list tbody").append('<tr class="feature-row" id="' + L.stamp(layer) + '" lat="' + layer.getLatLng().lat + '" lng="' + layer.getLatLng().lng + '"><td style="vertical-align: middle;"></td><td class="feature-name">' + layer.feature.properties.site_id + '</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
      }
    }
  });
  /* Update list.js featureList */
  featureList = new List("features", {
    valueNames: ["feature-name"]
  });
  featureList.sort("feature-name", {
    order: "asc"
  });
}

/* Basemap Layers */
var cartoLight = L.tileLayer.provider('OpenStreetMap.Mapnik');

var usgsImagery = L.layerGroup([L.tileLayer("http://basemap.nationalmap.gov/arcgis/rest/services/USGSImageryOnly/MapServer/tile/{z}/{y}/{x}", {
  maxZoom: 15,
}), L.tileLayer.wms("http://raster.nationalmap.gov/arcgis/services/Orthoimagery/USGS_EROS_Ortho_SCALE/ImageServer/WMSServer?", {
  minZoom: 16,
  maxZoom: 19,
  layers: "0",
  format: 'image/jpeg',
  transparent: true,
  attribution: "Aerial Imagery courtesy USGS"
})]);

/* Overlay Layers */
var highlight = L.geoJson(null);
var highlightStyle = {
  stroke: false,
  fillColor: "#00FFFF",
  fillOpacity: 0.2,
  radius: 10
};

 var hydro = omnivore.topojson('data/SingleLineWatercourseMerged.topojson').on('ready', function() {

  });
  var water = omnivore.topojson('data/Waterbodies_merged.topojson').on('ready', function() {

  });

    var roadStyles = {
    			"Expressway / Highway":{color:"orange", weight:4, opacity:0.5},
    			"Unpaved Surface":{color:"brown", weight:2, opacity:0.5, dashArray: [3, 10] },
    			"Collector":{color:"brown", weight:2, opacity:0.5},
    			"Local / Street":{color:"orange", weight:3, opacity:0.5},
    			"Winter":{color:"black", weight:3, opacity:0.5},
          "Access":{color:"green", weight:1, opacity:0.5}
    			};
    var roads = L.geoJson(null, {
            style: function (feature) {
                return roadStyles[feature.properties.NATRDCLASS];
            },
            onEachFeature: function (feature, layer) {

            }
          });
    var wcan = omnivore.topojson('data/WCAN_Roads_AOI.topojson').on('ready', function() {
      this.eachLayer(function(dist){
        roads.addData(dist.feature);
      })
    });

    var access =  omnivore.topojson('data/Demo_Access.topojson').on('ready', function() {
        this.eachLayer(function(dist){
          dist.feature.properties.NATRDCLASS = "Access";
          roads.addData(dist.feature);
        })
    });

/* Single marker cluster layer to hold all clusters */
var markerClusters = L.markerClusterGroup({
  spiderfyOnMaxZoom: true,
  showCoverageOnHover: false,
  zoomToBoundsOnClick: true,
  disableClusteringAtZoom: 12
});

/* Empty layer placeholder to add to layer control for listening when to add/remove wellsites to markerClusters layer */
var wellsiteLayer = L.geoJson(null);
var wellsites = L.geoJson(null, {
  pointToLayer: function (feature, latlng) {
    var myIcon = L.divIcon({className:"id-"+feature.properties.site_id});
    var marker = L.marker(latlng, {icon: myIcon});
    return marker;
  },
  onEachFeature: function (feature, layer) {
    if (feature.properties) {
      layer.on({
        click: function (e) {
          activeFeature = e.target;
          $("#feature-title").html(feature.properties.site_id);

          $("#feature-info").html( dialogContent(feature) );
          $("#featureModal").modal("show");
          highlight.clearLayers().addLayer(L.circleMarker([feature.geometry.coordinates[1], feature.geometry.coordinates[0]], highlightStyle));
        }
      });
      $("#feature-list tbody").append('<tr class="feature-row" id="' + L.stamp(layer) + '" lat="' + layer.getLatLng().lat + '" lng="' + layer.getLatLng().lng + '"><td style="vertical-align: middle;"></td><td class="feature-name">' + layer.feature.properties.site_id + '</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
      wellsiteSearch.push({
        name: layer.feature.properties.site_id,
        lease_type: layer.feature.properties.lease_type,
        source: "WellSites",
        id: L.stamp(layer),
        lat: layer.feature.geometry.coordinates[1],
        lng: layer.feature.geometry.coordinates[0]
      });
    }
  }
});
//TODO: set random statuses?
$.getJSON("data/demo_drill_events.json", function (data) {
  var sorted = data.features.sort(function(a, b){
    return a.properties.site_id.localeCompare(b.properties.site_id);
  });
  var last_event = {properties:""};
  var filtered = sorted.filter(function(event){
    var duplicate = event.properties.site_id == last_event.properties.site_id;
    let status = statuses[Math.floor(Math.random()*6)];
    if( duplicate ){
      last_event.properties.events.push(
        {lease_type:event.properties.lease_type, status:status});
    }
    else{
      event.properties.events = [{lease_type:event.properties.lease_type, status:status}];
    }
    last_event = event;
    return !duplicate;
  }
);
  //iterate through the sorted list and convert the events into a properties.event array
  wellsites.addData(filtered);
  map.addLayer(wellsiteLayer);
  siteData = filtered;
  initSiteSymbols();

});

map = L.map("map", {
  zoom: 9,
  center: [57.7, -111.6],
  layers: [cartoLight, hydro, roads, markerClusters, highlight],
  zoomControl: false,
  attributionControl: false
});

/* Layer control listeners that allow for a single markerClusters layer */
map.on("overlayadd", function(e) {

  if (e.layer === wellsiteLayer) {
    markerClusters.addLayer(wellsites);
    syncSidebar();
  }
});

map.on("overlayremove", function(e) {
  if (e.layer === wellsiteLayer) {
    markerClusters.removeLayer(wellsites);
    syncSidebar();
  }
});

/* Filter sidebar feature list to only show features in current map bounds */
map.on("moveend", function (e) {
  syncSidebar();
  initSiteSymbols()
});

/* Clear feature highlight when map is clicked */
map.on("click", function(e) {
  highlight.clearLayers();
});


var zoomControl = L.control.zoom({
  position: "bottomright"
}).addTo(map);

/* GPS enabled geolocation control set to follow the user's location */
var locateControl = L.control.locate({
  position: "bottomright",
  drawCircle: true,
  follow: true,
  setView: true,
  keepCurrentZoomLevel: true,
  markerStyle: {
    weight: 1,
    opacity: 0.8,
    fillOpacity: 0.8
  },
  circleStyle: {
    weight: 1,
    clickable: false
  },
  icon: "fa fa-location-arrow",
  metric: false,
  strings: {
    title: "My location",
    popup: "You are within {distance} {unit} from this point",
    outsideMapBoundsMsg: "You seem located outside the boundaries of the map"
  },
  locateOptions: {
    maxZoom: 18,
    watch: true,
    enableHighAccuracy: true,
    maximumAge: 10000,
    timeout: 10000
  }
}).addTo(map);

/* Larger screens get expanded layer control and visible sidebar */
if (document.body.clientWidth <= 767) {
  var isCollapsed = true;
} else {
  var isCollapsed = false;
}

var baseLayers = {
  "Street Map": cartoLight,
  "Aerial Imagery": usgsImagery
};

var groupedOverlays = {
  "Points of Interest": {
    "WellSites": wellsiteLayer
  },
  "Reference": {
    "Hydro": hydro,
    "Roads": roads
  }
};

L.control.mousePosition().addTo(map);
var legend = new FrontierLegend();
legend.addTo("legendModal");

var layerControl = L.control.groupedLayers(baseLayers, groupedOverlays, {
  collapsed: isCollapsed
}).addTo(map);

/* Highlight search box text on click */
$("#searchbox").click(function () {
  $(this).select();
});

/* Prevent hitting enter from refreshing the page */
$("#searchbox").keypress(function (e) {
  if (e.which == 13) {
    e.preventDefault();
  }
});

$("#featureModal").on("hidden.bs.modal", function (e) {
  $(document).on("mouseout", ".feature-row", clearHighlight);
});

/* Typeahead search functionality */
$(document).one("ajaxStop", function () {
  $("#loading").hide();
  sizeLayerControl();
  /* Fit map to hydro bounds */
  map.fitBounds(wellsites.getBounds());
  featureList = new List("features", {valueNames: ["feature-name"]});
  featureList.sort("feature-name", {order:"asc"});

  var hydroBH = new Bloodhound({
    name: "Hydro",
    datumTokenizer: function (d) {
      return Bloodhound.tokenizers.whitespace(d.name);
    },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    local: boroughSearch,
    limit: 10
  });


  var wellsitesBH = new Bloodhound({
    name: "WellSites",
    datumTokenizer: function (d) {
      return Bloodhound.tokenizers.whitespace(d.name);
    },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    local: wellsiteSearch,
    limit: 10
  });

  var geonamesBH = new Bloodhound({
    name: "GeoNames",
    datumTokenizer: function (d) {
      return Bloodhound.tokenizers.whitespace(d.name);
    },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
      url: "http://api.geonames.org/searchJSON?username=bootleaf&featureClass=P&maxRows=5&countryCode=US&name_startsWith=%QUERY",
      filter: function (data) {
        return $.map(data.geonames, function (result) {
          return {
            name: result.name + ", " + result.adminCode1,
            lat: result.lat,
            lng: result.lng,
            source: "GeoNames"
          };
        });
      },
      ajax: {
        beforeSend: function (jqXhr, settings) {
          settings.url += "&east=" + map.getBounds().getEast() + "&west=" + map.getBounds().getWest() + "&north=" + map.getBounds().getNorth() + "&south=" + map.getBounds().getSouth();
          $("#searchicon").removeClass("fa-search").addClass("fa-refresh fa-spin");
        },
        complete: function (jqXHR, status) {
          $('#searchicon').removeClass("fa-refresh fa-spin").addClass("fa-search");
        }
      }
    },
    limit: 10
  });
  hydroBH.initialize();
  wellsitesBH.initialize();
  geonamesBH.initialize();

  /* instantiate the typeahead UI */
  $("#searchbox").typeahead({
    minLength: 3,
    highlight: true,
    hint: false
  }, {
    name: "Hydro",
    displayKey: "name",
    source: hydroBH.ttAdapter(),
    templates: {
      header: "<h4 class='typeahead-header'>Hydro</h4>"
    }
  }, {
    name: "WellSites",
    displayKey: "name",
    source: wellsitesBH.ttAdapter(),
    templates: {
      header: "<h4 class='typeahead-header'><img src='assets/img/wellsite.png' width='24' height='28'>&nbsp;WellSites</h4>",
      suggestion: Handlebars.compile(["{{name}}<br>&nbsp;<small>{{lease_type}}</small>"].join(""))
    }
  }, {
    name: "GeoNames",
    displayKey: "name",
    source: geonamesBH.ttAdapter(),
    templates: {
      header: "<h4 class='typeahead-header'><img src='assets/img/globe.png' width='25' height='25'>&nbsp;GeoNames</h4>"
    }
  }).on("typeahead:selected", function (obj, datum) {
    if (datum.source === "Hydro") {
      map.fitBounds(datum.bounds);
    }

    if (datum.source === "WellSites") {
      if (!map.hasLayer(wellsiteLayer)) {
        map.addLayer(wellsiteLayer);
      }
      map.setView([datum.lat, datum.lng], 17);
      if (map._layers[datum.id]) {
        map._layers[datum.id].fire("click");
      }
    }
    if (datum.source === "GeoNames") {
      map.setView([datum.lat, datum.lng], 14);
    }
    if ($(".navbar-collapse").height() > 50) {
      $(".navbar-collapse").collapse("hide");
    }
  }).on("typeahead:opened", function () {
    $(".navbar-collapse.in").css("max-height", $(document).height() - $(".navbar-header").height());
    $(".navbar-collapse.in").css("height", $(document).height() - $(".navbar-header").height());
  }).on("typeahead:closed", function () {
    $(".navbar-collapse.in").css("max-height", "");
    $(".navbar-collapse.in").css("height", "");
  });
  $(".twitter-typeahead").css("position", "static");
  $(".twitter-typeahead").css("display", "block");
});

// Leaflet patch to make layer control scrollable on touch browsers
var container = $(".leaflet-control-layers")[0];
if (!L.Browser.touch) {
  L.DomEvent
  .disableClickPropagation(container)
  .disableScrollPropagation(container);
} else {
  L.DomEvent.disableClickPropagation(container);
}
var activeFeature;

function setStatus(status) {
  changeStatus( activeFeature.feature, status);//leaflet layer
  var site = getSite(activeFeature.feature.properties.site_id);
  changeStatus( site, status);//d3js json data
  initSiteSymbols();
}

function changeStatus( site, status){
  site.properties.events.forEach(function( event){
    if( event.lease_type === status.name)
      event.status = status.value;
  })
}

function getSite( id){
  for( site in siteData)
    if( siteData[site].properties.site_id === id)
      return siteData[site]
}

//want to do a join without adding any elements
//only want to add svg when it doesn't exist
//so, set the class of the div then select all the divs with that class
// need to add a key to the data join
//d3 colour scale
function initSiteSymbols() {
  var sites = d3.selectAll("div.leaflet-marker-icon")
    .data(siteData, function(d) {
      var id = getIDFromClass(this);
      return d ? d.properties.site_id : id;
    });
    sites.selectAll("svg").remove();
    sites.append("svg").attr("width",20)
			.attr("height",20);
      var svg = sites.select("svg");
      svg.append("g")
.selectAll("path").data(
        function(d){
           return d.properties.events;
        }).enter().append("path")
      .attr("d",  d3.symbol().type(function(d){
        console.log( d.lease_type);
        return leaseSymbols(d.lease_type);}))
      .attr("transform", "translate("+ 10 +"," + 10 + ")")
      .style("stroke", function(d){return statusColour( d.status);})
      .style("fill", function(d){return statusColour( d.status);})
      .attr("fill-opacity", OPACITY);
  	}

    function getIDFromClass( element){
      if( typeof element.className == "undefined") return "";
      var classes = element.className.split(' ');
      for ( var i = 0; i < classes.length; i++) {
        if (classes[i].indexOf("id-") == 0) {
          return classes[i].substring(3);
        }
      }
    }

function dialogContent(feature) {
  var content = "<table class='table table-striped table-bordered table-condensed'>" +
    "<tr><th>Name</th><td>" +
    feature.properties.site_id + "</td></tr>";

  feature.properties.events.forEach(function(event) {
    content += "<tr><th>Lease</th><td>" + event.lease_type + "</td></tr>";
    content += "<tr><th>Status</th><td>";

    statuses.forEach(function(status) {
      content += "<input type='radio' name='"+event.lease_type+"' onChange='setStatus(this)' value='" + status + "' ";
      if (status.toLowerCase() === event.status.toLowerCase())
        content += " checked ";
      content += " >" + status + "<br>";
    })
    content += "</td></tr>";
  })

  content += "<tr><th>Job #</th><td>" + feature.properties.job_num + "</td></tr>";
  content += "<tr><th>Documents</th><td><a class='url-break' href='" + feature.properties.URL;
  content += "' target='_blank'>" + feature.properties.URL + "</a></td></tr>";
  content += "</table>";
  return content;
}
