function FrontierShapes() {}

FrontierShapes.prototype.draw = function( g, x, y, events) {
  if( !events) return;
  events.forEach( function( event){
    this.drawSonic(g, x, y, "None", " Black");
  })
}

FrontierShapes.prototype.drawSonic = function(g, x, y, stroke, fill) {
  g.append("circle").attr("cy", 0).attr("cx", 0)
    .attr("r", 5)
    .attr("transform", "translate("+ x +"," + y + ")")
    .style("stroke", stroke)
    .style("fill", fill)
    .attr("fill-opacity", OPACITY);
}

FrontierShapes.prototype.drawAug = function(g, x, y, stroke, fill) {
  g.append("path")
    .attr("d", d3.symbol().type(d3.symbolTriangle))
    .attr("transform", "translate("+ x +"," + y + ")")
    .style("stroke", stroke)
    .style("fill", fill)
    .attr("fill-opacity", OPACITY);
}

FrontierShapes.prototype.drawCH = function(g, x, y, stroke, fill) {
  g.append("rect")
    .attr("y", -6)
    .attr("x", -6)
    .attr("rx", 4)
    .attr("ry", 4)
    .attr("width", 12)
    .attr("height", 12)
    .style("stroke", stroke)
    .attr("transform", "translate("+ x +"," + y + ")")
    .style("fill", fill)
    .attr("fill-opacity", OPACITY);
}
