var DATA_START = 2;
var PROB_COLUMN = 20;
const points = [1, 3, 5, 10];

var picks;
var games;
var div = d3.select("body").append("div")
  .attr("class", "tooltip")
  .style("opacity", 0);

readGames();

function readGames() {
  d3.text("games2022.csv", function(text) {
    games = d3.csv.parseRows(text)
    readPicks();
  });
}

function readPicks() {
  d3.text("picks.csv", function(data) {
    var oldpicks = d3.csv.parseRows(data);
    picks = [];
    oldpicks.forEach(function(d, i) {
      d = d.slice(0, PROB_COLUMN);
      if (i > 0)
        d.push(0); //probability column
      else {
        d.push("P%");
      }
      picks.push(d);
    });
    readSimulations(picks);
  });
}
var probMap = {};

function readSimulations(picks) {
  d3.csv("simulations_recent.csv", function(data) {
    let probabilities = data.filter(d => d.scenerio == "ALL");
    probabilities = probabilities.map(row => {
      var wins = [];
      wins.push(Number(row.round2));
      wins.push(Number(row.round3));
      wins.push(Number(row.round4));
      wins.push(Number(row.wonCup));
      return {
        team: row.teamCode,
        win: wins
      };
    });
    probabilities.forEach(p => probMap[p.team] = p.win);
    calcProbabilityOfWin(picks);
    render(picks, probabilities);
  });
}

function render(picks, prob1) {
  renderTable(picks, prob1);
  renderCircles(picks);
}

function renderTable(picks, prob1) {
  var headerData = picks[0];
  var picks_table = d3.select("#picks")
    .append("table")
    .attr("class", "table  table-condensed");
  picks_table.append("thead").append("tr")
    .selectAll("th")
    .data(headerData)
    .enter().append("th")
    .text(d => d);
  var sortedPicks = picks.slice(1).sort((a, b) => b[PROB_COLUMN] - a[PROB_COLUMN]);

  var rows = picks_table.append("tbody").selectAll("tr")
    .data(sortedPicks).enter();

  rows.append("tr")
    .selectAll("td")
    .data(d => d).enter()
    .append("td")
    .text((d, i) => i < PROB_COLUMN ? d : Number(d, i).toFixed(1))
    .attr("class", (d, i) => oddsColor(d, calculateRound(i-2)));

  var table = d3.select("#stats").append('table').attr("class", "table table-condensed");
  var thead = table.append('thead')
  var tbody_stats = table.append('tbody');
  var columns = ["team", "win"];

  // append the header row
  thead.append('tr')
    .selectAll('th')
    .data(columns).enter()
    .append('th')
    .text(column => column);

  // create a row for each object in the data
  var rows = tbody_stats.selectAll('tr')
    .data(prob1.sort((a, b) => b.win[2] - a.win[2])
      .filter(d => d.win[2] > 0))
    .enter()
    .append('tr');

  // create a cell in each row for each column
  var cells = rows.selectAll('td')
    .data((teamData) => {
      const {
        team,
        win
      } = teamData;
      const teamAndWins = [team, ...win.map(w => `${team},${w}`)];
      return teamAndWins;
    })
    .enter()
    .append('td')
    .text((d, i) => i > 0 ? d.split(',')[1] : d)
    .attr("class", (d, i) => i > 0 ? oddsColor(d.split(',')[0], i - 1) : "");
}

function renderCircles(data) {
  data = data.filter(d => d[PROB_COLUMN] > 0).map(function(d) {
    d.value = d[PROB_COLUMN];
    return d;
  });
  //bubbles needs very specific format, convert data to this.
  var color = d3.scale.category10(); //color category
  var diameter = 300; //max size of the bubbles

  var svg = d3.select("#chart")
    .append("svg")
    .attr("width", diameter)
    .attr("height", diameter)
    .attr("class", "bubble");

  var bubble = d3.layout.pack()
    .sort(null)
    .size([diameter, diameter])
    .padding(1.5);

  var nodes = bubble.nodes({
    children: data
  }).filter(function(d) {
    return !d.children;
  });

  //setup the chart
  var bubbles = svg.append("g")
    .attr("transform", "translate(0,0)")
    .selectAll(".bubble")
    .data(nodes.filter(function(d) {
      return d.winProbability > 0.01;
    }))
    .enter();

  //create the bubbles
  bubbles.append("circle")
    .attr("r", d => d.r)
    .attr("cx", d => d.x)
    .attr("cy", d => d.y)
    .style("fill", d => color(d[0]))
    .on("mouseover", function(d) {
      div.transition()
        .duration(200)
        .style("opacity", .9);
      div.html(waysToWinHTML(d))
        .style("left", (d3.event.pageX) + "px")
        .style("top", (d3.event.pageY - 28) + "px");
    })
    .on("mouseout", function(d) {
      div.transition()
        .duration(200)
        .style("opacity", 0);
    });;

  //format the text for each bubble
  bubbles.append("text")
    .attr("x", d => d.x)
    .attr("y", d => d.y)
    .attr("text-anchor", "middle")
    .html(d => d[0].split("/").join("-"))
    .on("mouseover", function(d) {
      div.transition()
        .duration(200)
        .style("opacity", .9);
      div.html(waysToWinHTML(d))
        .style("left", (d3.event.pageX) + "px")
        .style("top", (d3.event.pageY - 28) + "px");
    })
    .on("mouseout", function(d) {
      div.transition()
        .duration(200)
        .style("opacity", 0);
    })
    .style({
      "font-family": "Helvetica Neue, Helvetica, Arial, san-serif",
      "font-size": function(d) {
        return Math.min(2 * d.r, (2 * d.r - 8) / this.getComputedTextLength() * 13) + "px";
      }
    });
}
//content for tooltip popup
function waysToWinHTML(list) {
  const topWays = Math.min(10, list.wins.length);
  const sortedWays = list.wins.sort((a, b) => b[15] - a[15]).slice(0, 10);

  const rows = sortedWays.map(s => {
    const cells = s.map(d => `<td nowrap>${d}</td>`).join('');
    return `<tr>${cells}</tr>`;
  }).join('');

  return `
    <h3>Ways to win or tie: ${list.wins.length} TOP ${topWays}</h3>
    <table class='table ways'>
      ${rows}
    </table>
  `;
}

function calcProbabilityOfWin(picks) {
  for (var j in picks) {
    picks[j].gameGuess = 0;
  }
  var totalP = 0;
  const possibles = bracketPossibilities();
  possibles.forEach(p => {
    const prob = probabilityOfWinners(p);
    totalP += prob;
    updateWinners(p, prob);
  })
  console.log("Number of possibilities: " + possibles.length + " (should equal 32768 ), total probability " + totalP + " (should equal 1.0 )");
}

function bracketPossibilities(){
  const brackets = [];
  games[0].forEach(function(a) {
    games[1].forEach(function(b) {
      games[2].forEach(function(c) {
        games[3].forEach(function(d) {
          games[4].forEach(function(e) {
            games[5].forEach(function(f) {
              games[6].forEach(function(g) {
                games[7].forEach(function(h) {

                  games[8].forEach(function(ab) {
                    if (ab !== a && ab != b) return;
                    games[9].forEach(function(cd) {
                      if (cd !== c && cd != d) return;
                      games[10].forEach(function(ef) {
                        if (ef !== e && ef != f) return;
                        games[11].forEach(function(gh) {
                          if (gh !== g && gh != h) return;

                          games[12].forEach(function(abcd) {
                            if (abcd !== ab && abcd != cd) return;
                            games[13].forEach(function(efgh) {
                              if (efgh !== ef && efgh != gh) return;

                              games[14].forEach(function(abcdefgh) {
                                if (abcdefgh !== abcd && abcdefgh != efgh) return;
                                var winners = [a, b, c, d, e, f, g, h,
                                  ab, cd, ef, gh, abcd, efgh, abcdefgh
                                ];
                                brackets.push (winners);
                              });
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });
return brackets;
}

function updateWinners(winners, prob) {
  var poolWinnerList = findPoolWinner(winners);
  winners.push(prob.toFixed(5))

  for (var pi in poolWinnerList) {
    var index = parseInt(poolWinnerList[pi]);
    var pick = picks[index]
    pick[PROB_COLUMN] += prob * 100.0;
    pick.winProbability = picks[index][PROB_COLUMN];
    if (!pick.wins) {
      pick.wins = [];
    }
    if (prob > 0.0001) {
      pick.wins.push(winners);
    }
  }
}

//should the current "number of games guesses" be included here?
function findPoolWinner(winners) {
  var winnerList = [];
  var count = []; //track the "score" for each entry for this combination of winners
  for (j in picks) {
    count[j] = picks[j].gameGuess;
  }
  for (var i = 0; i < winners.length; i++) {
    const round = calculateRound(i);
    var pts = points[round];
    for (var t = 1; t < picks.length; t++) {
      var pick = picks[t];
      var pickVal = pick[DATA_START + i];
      if (pickVal == winners[i]) {
        count[t] += pts;
      }
    }
  }

  var maxval = 0;
  for (var j = 1; j < picks.length; j++) {
    if (count[j] > maxval) {
      maxval = count[j];
      winnerList = [];
    }
    if (count[j] == maxval) {
      winnerList.push(j);
    }
  }
  return winnerList;
}

function calculateRound(index) {
  if (index < 8) return 0;
  if (index < 12) return 1;
  if (index < 14) return 2;
  return 3;
}

function probabilityOfWinners(winners) {
  let processedWinners = new Set();

  return winners.reduceRight((total_probability, winner, index) => {
    let round = calculateRound(index);

    if (!processedWinners.has(winner)) {
      processedWinners.add(winner);
      return total_probability * getProbability(winner, round);
    }

    return total_probability;
  }, 1);
}

function getProbability(team, round) {
  return probMap[team][round];
}

function oddsColor(team, round) {
  if (!probMap[team]) return "";
  var w = probMap[team][round];
  if (w == 0) return "disabled";
  else if (w == 1) return "won";
  else if (w < 0.20) return "danger";
  else if (w < 0.50) return "warning";
  else if (w < .80) return "info";
  else return "success";
}
