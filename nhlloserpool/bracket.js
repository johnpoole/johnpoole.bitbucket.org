var logoheight = 80;
var diameter = 500,
    format = d3.format(".2f"),
    color = d3.scale.category20c();
var width = 600, height = 800
var bubble = d3.layout.pack()
    .sort(null)
    .size([diameter, diameter])
    .padding(1.5);

var svg = d3.select("#pool").append("svg")
    .attr("width", width)
    .attr("height", height)
    .attr("class", "bubble");

function buildtree(teams) {
  var round = 4;//change
  var gid = 15;//2^round-1

  var root = {
    gid: gid--,
    region: "atlantic-metropolitan-central-pacific",
    round: round--,
    children: [],
  };

  var roundgames = {4: [root]};

  // 1-16: south; 17-32: east; 33-48: west; 49-64: midwest
  // 65-72: south; 73-80: east; 81-88: west; 89-96; midwest
  // 97-100: south; 101-104: east; 105-108: west; 109-112: midwest
  // 113-114: s; 115-116: e; 117-118: w; 119-120: mw
  // 121: s; 122: e; 123: w; 124: mw
  // 125: s-e; 126: w-mw
  // 127: s-e-w-mw
  function region(gid) {//change to reflect rounds and gid max
    if ((gid >= 1 && gid <= 2)  ||
        (gid == 9)) { return "atlantic"; }
    if ((gid >= 3 && gid <= 4) ||
        ( gid == 10)) { return "metropolitan"; }
    if ((gid >= 5 && gid <= 6) ||
        ( gid == 11)) { return "central"; }
    if ((gid >= 7 && gid <= 8) ||
        ( gid == 12)) { return "pacific"; }
    if (gid == 13) { return "atlantic-metropolitan"; }
    if (gid == 14) { return "central-pacific"; }
    if (gid == 15) { return "atlantic-metropolitan-central-pacific"; }

    // raise an error if we fall through
    throw new Error("undefined region for gid " + gid);
  }

  while (round > 0) {
    roundgames[round] = [];
    for (var i=0; i < roundgames[round+1].length; i++) {
      var left = {
        gid: gid,
        region: region(gid),
        round: round,
        children: [],
      };
      gid--;

      var right = {
        gid: gid,
        region: region(gid),
        round: round,
        children: [],
      };
      gid--;

      roundgames[round+1][i].children.push(left);
      roundgames[round+1][i].children.push(right);
      roundgames[round].push(left);
      roundgames[round].push(right);
    }
    round--;
  }

  //change to match order with fewer seeds
  var r_to_l = ['1',  '2'];
  var l_to_r = [ "2", "1"];
  var regions = ["atlantic", "metropolitan", "central", "pacific"];

  function findgame(gid) {
    var found;

    $.each(roundgames[1], function(i, game) {
      if (game.gid == gid) {
        found = game;
        return false;
      }
    });

    if (!found) throw new Error("Unable to find gid " + gid);

    return found;
  }

  var gid = 1;
  $.each(regions, function(i, region) {
    var order;
    if (region == "metropolitan" || region == "pacific") { 
		order = r_to_l; 
	}
    else{ 
		order = l_to_r; 
	}

    $.each(order, function(j, seed) {
      var game = findgame(gid);
      game.team = teams[region][seed];
      gid++;
    });
  });

  return root;
}


function main(teamsOld) {

	var teams = updateProb(teamsOld, probabilities);
	var root = buildtree(teams);

  var partition = d3.layout.partition()
    .sort(null)
    .size([2 * Math.PI, radius]) // x maps to angle, y to radius
    .value(function(d) { return 1; }); //Important!

  var arc = d3.svg.arc()
    .startAngle(function(d) { return d.x; })
    .endAngle(function(d) { return d.x + d.dx; })
    .innerRadius(function(d) { return d.y; })
    .outerRadius(function(d) { return d.y + d.dy; });

 
  var svg = d3.select('#bracket')
              .append('svg')
                .attr('width', radius*2+25)
                .attr('height', radius*2+25)
              .append('g')
                .attr("id", "center")
                .attr('transform', trans(xCenter,yCenter));

  var chart = svg.append('g').attr("id", "chart");
  chart.datum(root).selectAll('.arc')
    .data(partition.nodes)
    .enter()
    .append('g')
      .attr("class", "arc")
      .attr("id", function(d) { return "game" + d.gid; });

  var arcs = d3.selectAll('.arc');

  function clamp(n) {
    if (n>1) { return 1; }
    return n;
  }

  function calcTextcolor(color, alpha) {
    // http://javascriptrules.com/2009/08/05/css-color-brightness-contrast-using-javascript/
    var brightness = color[0]*0.299 + color[1]*0.587 + color[2]*0.114;
    brightness /= alpha;

    if (brightness > 125 || alpha < 0.15) {
      return "#333"; //black
    }
    return "#FFF"; //white
  }

  function rgba(color, alpha) {
    // Very small alpha values mess up webkit
    if (alpha.toString().indexOf("e") > -1) { alpha = 0; }
    return "rgba("+color[0]+","+color[1]+","+color[2]+","+alpha+")";
  }

  var spots = {
   
    15: [0,20]
  };

  function fillpath(game) {
    var par = game.parent;
    for (var round=game.round; round < 4; round++) {//change to match rounds
      var sr = "round"+round;
      var gameg = d3.select("#game" + par.gid);
      if (gameg.datum().team) { par = par.parent; continue; }

      // color the main path
      var alpha = clamp(game.team[sr]*2);
      var color = rgba(game.team.color, alpha);
      gameg.select("path").style("fill", color);

      var x,y;
      if (spots.hasOwnProperty(par.gid)) {
        x = spots[par.gid][0];
        y = spots[par.gid][1];
      } else {
        var bb = gameg.node().getBBox();
        x = bb.x + bb.width/2;
        y = bb.y + bb.height/2;
      }

      var pct = (game.team[sr] * 100);
      if (pct > 1)      { pct = pct.toFixed(0).toString() + "%"; }
      else if (pct > 0) { pct = "<1%"; }
      else              { pct = ""; }
      gameg.append("text")
          .text(pct)
          .attr("class", "pcttext")
          .attr("fill", calcTextcolor(game.team.color, alpha))
          .attr("text-anchor", "middle")
          .attr("x", x)
          .attr("y", y);
      par = par.parent;
    }

    var teamcolor = calcTextcolor(game.team.color, alpha);
    d3.select("#center")
      .append("text")
      .attr("x", 0)
      .attr("y", 0)
      .attr("text-anchor", "middle")
      .style("fill", "#666")
      .attr("id", "teamname")
      .text(game.team.name);

    d3.selectAll("#game127 .logo").style("opacity", "0.1");
  }

  function getLogoColors(game) {
    RGBaster.colors("../logos/"+game.team.name+".png", function(payload) {
      var colors = payload.dominant.match(/(\d{1,3}),(\d{1,3}),(\d{1,3})/);

      game.team.color = [colors[1], colors[2], colors[3]];

      fillpath(game);
    });
  }

  function hover(game) {
    if (!game.team) { return; }

    // If we don't yet know the team's color, parse the logo and save it.
    // Else, just fill the path with the cached value
    if (game.team.color === undefined) {
      getLogoColors(game);
    } else {
      fillpath(game);
    }
  }

  function clear(team) {
    d3.selectAll(".arc path").style("fill", "#fff");
    d3.selectAll(".pcttext").remove();
    d3.selectAll("#teamname").remove();
    d3.selectAll("#game127 .logo").style("opacity", "1");
  }

  arcs.on('mouseenter', function(d) {
	  clear(d); hover(d); 
	  })
    .on('mouseleave', function(d) { clear(d); })
    .on('touchstart', function(d) { clear(d); hover(d); })
    .append('path')
      .attr('d', arc)
      .attr("id", function(d) { return "path-game" + d.gid; });

  arcs.append("clipPath")
    .attr("id", function(d) { return "text-clip-game" + d.gid; })
  .append("use")
    .attr("xlink:href", function(d) { return "#path-game" + d.gid; });

  logos = arcs.append('g')
    .attr("class", "logo")
    .attr("clip-path", function(d) { return "url(#text-clip-game"+d.gid+")"; })
    .attr("id", function(d) { return "logo" + d.gid; });

  logos.filter(function(d) {return d.team; })
    .append("image")
    .attr("xlink:href", function(d) { return "../logos/"+d.team.name+".png"; })
    .attr("transform", logo)
    .attr("width", logoheight)
    .attr("height", logoheight);

  showLogos();

  var nradius = radius + 30;
  var arcmaker = d3.svg.arc().innerRadius(nradius).outerRadius(nradius);
  var regionarcs = [
    {region: "Pacific", startAngle: 0, endAngle: Math.PI/2},
    {region: "Central", startAngle: Math.PI/2, endAngle: Math.PI},
    {region: "Atlantic", startAngle: Math.PI, endAngle: 3*Math.PI/2},
    {region: "Metropolitan", startAngle: 3*Math.PI/2, endAngle: 2*Math.PI}
  ];

  var namearcs = d3.select("#center")
    .append("g")
      .attr("id", "namearcs");

  var namearc = namearcs.selectAll("g")
    .data(regionarcs)
    .enter()
    .append("g")
      .attr("class", "namearc");

  namearc.append("defs").append("path")
      .attr("d", arcmaker)
      .attr("id", function(d) { return "regionpath-" + d.region; })
      .attr("class", "regionpath")
      //.attr("style", "display:none");

  namearc.append("text")
    .append("textPath")
      .attr("text-anchor", "middle")
      .attr("startOffset", "25%")
      .attr("xlink:href", function(d) { return "#regionpath-" + d.region; })
      .style("fill", "#888")
      .style("font-weight", "bold")
      .style("font-size", "20px")
      .text(function(d) { return d.region; });
}
function showLogos(){
	for (var i=1; i < 16; i++) {//change to match max gid
		var game = d3.select("#game" + i).datum();
		if (game.team && game.team["round" + game.round] == 1) {
			var gid = game.parent.gid;
			var wingame = d3.select("#game" + gid);
			wingame.datum().team = game.team;
			wingame.append('g')
			.attr("class", "logo")
			.attr("clip-path", clipurl)
			.attr("id", logoid)
			.append("image")
			.attr("xlink:href", logoname)
			.attr("transform", logo)
			.attr("width", logoheight)
			.attr("height", logoheight);
    }
  }
}
function showLogosforPlayer(player){
	for (var i=1; i < 15; i++) {//change to match max gid
		var game = d3.select("#game" + i).datum();
		var round = game.round;
		if (game.team && player.picks[ game.round-1]&& player.picks[ game.round-1].indexOf(game.team.name)>=0){
			var gid = game.parent.gid;
			var wingame = d3.select("#game" + gid);
			wingame.datum().team = game.team;
			wingame.select('g')
			.attr("class", "logo")
		//	.attr("clip-path", clipurl)
			.attr("id", logoid)
			.append("image")
			.attr("xlink:href", logoname)
			.attr("transform", logo)
			.attr("width", logoheight)
			.attr("height", logoheight);
    }
  }
}
function clearLogos(){
	for (var i=1; i < 16; i++) {//change to match max gid
		var game = d3.select("#game" + i).datum();
		if (game.parent ){
			var gid = game.parent.gid;
			var wingame = d3.select("#game" + gid);
			wingame.datum().team = undefined;
			wingame.selectAll('g').selectAll('image').remove();		
		}
	}
}
var teamData;
var probabilities;
var root = {"name":"pool"};

queue()
  .defer(d3.json, 'teams.json')
  .await(function(err, teams) {
  
  d3.xml("Probabilities.xml", function(error, data) {
		data = [].map.call(data.querySelectorAll("tr"), function(row) {
			if( row.children.length <= 9){
				return {};
			}
		return {
			team: row.children[0].firstChild.innerHTML.replace('%',"").replace("X","0"),
			round3:row.children[7].firstChild.innerHTML.replace('%',"").replace("X","0")/100,
			round2:row.children[8].firstChild.innerHTML.replace('%',"").replace("X","0")/100,
			round1:row.children[9].firstChild.innerHTML.replace('%',"").replace("X","0")/100,
			conference:row.children[10].firstChild.innerHTML.replace('%',"").replace("X","0")/100,

		};		
  });
  probabilities = data;
	var possibleOutcomes = generateOutcomes( teams );
	
	main(teams); 
	teamData = teams;
	d3.json("picks.json", function(error, picks) {
		if (error) throw error;
		root.children = [];
		picks.users.forEach( calcScore, teamData);
		forceChart(root);
	});
  });
  });

  /* calculate a score for a given set of outcomes */
function calcScore( user ){
	var outcomes = this;
	var score={};
	score.name = user.name;
	score.size = 0;
	score.description = "";
	score.picks = [];
	user.rounds.forEach( function ( round, i){
		var picks = round.picks.split(',').sort();
		score.description += " round"+(i+1);
		score.picks.push( round.picks); 
		picks.forEach( function(pick){
			for(var conference in outcomes) {
				var stat = outcomes[conference]; 
				for( var prob in stat){
					var points = [1,2,3];
					if(stat[prob].name == pick){
						var index = 'round'+(i+1);
						var pointNum = points[i];
						var statNum = stat[prob][index];
						var add = points[i]*stat[prob][index];
						score.size+=add;
						score.description += " "+pick +" = "+ d3.format(".2f")(add);
					}
				}
			}
		});
		score.description +="\n";
	});
	root.children.push(score);
}
//render the d3 forcechart
function forceChart( root ){
	var color = d3.scale.category20();
	var force = d3.layout.force()
    .charge(-600)
    .linkDistance(150)
    .size([width, height]);

	var graph = createForceData( root );

	force.linkStrength(function(link) {
		return link.value;       
    });
	force
      .nodes(graph.nodes)
      .links(graph.links)
      .start();

	var link = svg.selectAll(".link")
		.data(graph.links)
		.enter().append("line")
		.attr("class", "link")
		.style("stroke-width", function(d) { return d.value* d.value/4; });

	var node = svg.selectAll(".node")
		.data(graph.nodes)
		.enter().append("g")
		.call(force.drag);
	  
	  
	node.append("circle").
	attr("class", "node")
      .on('mouseenter', function(d) {
		showLogosforPlayer(d);
	  })
	  .on('mouseleave', function(d) {
		  clearLogos();
	  })
      .attr("r", function(d) { return d.size*d.size*2; })
      .style("fill", function(d) { 
		return color(d.name); 
	  });
	  
	  node.append("text")
      .attr("dy", ".3em")
      .style("text-anchor", "middle")
      .text(function(d) { 
		return d.name; 
	  });
 
	node.append("title")
      .text(function(d) { 
	  return format(d.value)+"\n"+d.description; 
	  });

	force.on("tick", function() {
		link.attr("x1", function(d) { return d.source.x; })
        .attr("y1", function(d) { return d.source.y; })
        .attr("x2", function(d) { return d.target.x; })
        .attr("y2", function(d) { return d.target.y; });

		  node.attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });
	});
}
// Returns a flattened hierarchy containing all leaf nodes under the root.
function classes(root) {
  var classes = [];

  function recurse(name, node) {
    if (node.children) node.children.forEach(function(child) { 
		recurse(node.name, child); 
	});
    else 
		classes.push({packageName: name, className: node.name, value: node.size, description: node.description, picks:node.picks});
  }

  recurse(null, root);
  return {children: classes};
}

function createForceData( root ){
	var data = {nodes:[],links:[]};

	root.children.forEach( function(user, i){
		data.nodes.push({"name":user.name, "size":user.size, value: user.size, "description":user.description, "picks":user.picks});
		
			root.children.forEach( function (other, j ){
				var linkValue = calcLinkValue(user, other);

				if( linkValue > 5 ){
					data.links.push({"source":i, "target":j, "value":linkValue});			
				}
			});
		});
	return data;
}
function calcLinkValue(user, other){
		var value = 0
		for(var pickNum in user.picks) { 
			var picksName = user.picks[pickNum].split(',');
			for( var name in picksName ){
				var picksOtherName = other.picks[pickNum].split(',');
				for( var otherName in picksOtherName ){
					if( picksName[name] == picksOtherName[otherName]){
						value+=Number(pickNum)+1;
					}
				}
			}
		}
		return value;
}

//bubble chart 
function bubbleChart(root){
  var node = svg.selectAll(".node")
      .data(bubble.nodes(classes(root))
      .filter(function(d) { return !d.children; }))
    .enter().append("g")
      .attr("class", "node")
      .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });

  node.
  append("title")
      .text(function(d) { 
	  return d.className + ": " + format(d.value)+"\n"+d.description; 
	  });

  node.append("circle")
  .on('mouseenter', function(d) {
		showLogosforPlayer(d);
	  })
	  .on('mouseleave', function(d) {
		  clearLogos();
	  })
      .attr("r", function(d) { return d.r; })
      .style("fill", function(d) { 
		return color(d.className); 
	  });

  node.append("text")
      .attr("dy", ".3em")
      .style("text-anchor", "middle")
      .text(function(d) { return d.className.substring(0, d.r / 3); });
	  
	  d3.select(self.frameElement).style("height", diameter + "px");

	//bubble chart end
}


  function clipurl(d)  { return "url(#text-clip-game"+d.gid+")"; }
  function logoname(d) { return "../logos/"+d.team.name+".png"; }
  function logoid(d)   { return "logo" + d.gid; }
  
  function logo(d) {
      var bb = d3.select("#game"+d.gid+" path").node().getBBox();
      var x = bb.x + bb.width/2;
      var y = bb.y + bb.height/2;
      if (multipliers.hasOwnProperty(d.round)) {
        var m = multipliers[d.round];
        x *= m;
        y *= m;
      }
      x -= logoheight/2;
      y -= logoheight/2;
      return trans(x, y);
  }
  var multipliers = {
	1: 1.05,
	2: 1.15,
	3: 1.4,
    4: 1.03,
    5: 1.15,
    6: 1.4,
  }

 function trans(x, y) {
    return 'translate('+x+','+y+')';
  }

  function rotate(a, x, y) {
    a = a * 180 / Math.PI;
    return 'rotate('+a+')';
  }
  var radius = 300,
      numRounds = 4,//match rounds constant
      segmentWidth = radius / (numRounds + 1);
  var xCenter = radius, yCenter = radius;
  
  	function updateProb( teamData, prob){
		for(var conference in teamData) { 
			var stat = teamData[conference]; 
			for( var val in stat){
				for( var p in prob ){
					if(prob[p].team && stat[val].name == prob[p].team){	
						stat[val].round1 = prob[p].round1;
						stat[val].round2 = prob[p].round2;
						stat[val].round3 = prob[p].round3;							
					}
				}
			}
		}
		return teamData;
	}
	
	function generateOutcomes( odds ){
		console.log(JSON.stringify(odds));
		for( var i in [1,2,3]){
			for(var conference in odds) {
				var stat = odds[conference]; 
				for( var prob in stat){
						var index = 'round'+[1,2,3][i];
						var statNum = stat[prob][index];						
				}
			}
		}
	}