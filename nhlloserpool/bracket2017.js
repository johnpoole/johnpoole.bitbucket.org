
var JASON_COLOUR = "#f79620";
var YEAR = "2018";
var DATA_START=2;
var NUMBER_OF_PICKS=3;
var PROB_COLUMN=8;	
var seriesGames = [5,7,0];

var picks;
var games;
var div = d3.select("body").append("div")	
    .attr("class", "tooltip")				
    .style("opacity", 0);
//todo: need functions to map column indexes to field for each dataset	
readGames();

function readGames(){
	d3.text("games"+YEAR+".csv", function(text){
		games = d3.csv.parseRows(text)
		readPicks();
	});
}

function readPicks(){
	d3.text("picks"+YEAR+".csv", function(data) {
		picks = d3.csv.parseRows(data);
		var header = picks[0];
		picks = picks.slice(1);
		picks.header = header;
		picks.header.push( "%Prob");
		picks.header.push( "Points Earned");

		picks.forEach( function(d){
			d.push(0); //probability column
		});
		readSimulations(picks);
	});
}

function readSimulations(picks){
	d3.csv("../nhlpool/simulations_recent.csv", function(probabilities) {
		 probabilities = [].map.call(probabilities.filter(function(d){ 
			return  d.scenerio == "ALL" //d.madePlayoffs === "1" &&
		}), function(row) {
			var probGame = [];
			probGame.push( Number(row.round3Winin4) + Number(row.round3Lossin4));
			probGame.push( Number(row.round3Winin5) + Number(row.round3Lossin5));
			probGame.push( Number(row.round3Winin6) + Number(row.round3Lossin6));
			probGame.push( Number(row.round3Winin7)+ Number(row.round3Lossin7));
			
			probGame.push( Number(row.round4Winin4) + Number(row.round4Lossin4));
			probGame.push( Number(row.round4Winin5) + Number(row.round4Lossin5));
			probGame.push( Number(row.round4Winin6) + Number(row.round4Lossin6));
			probGame.push( Number(row.round4Winin7)+ Number(row.round4Lossin7));
			var wins = [];
			wins.push( Number(row.round2));
			wins.push( Number(row.round3));
			wins.push( Number(row.round4));
			wins.push( Number(row.wonCup));
			return {
			 gamesHome: 3,//Number(gameText[0]),
			 gamesAway: 2,//Number(gameText[1]),
			 gameNumChance: probGame,
			 team: row.teamCode,
			 win:wins,
			 row:row
			};		
		});
		calcProbabilityOfWin(picks, probabilities);
		pointsSoFar( picks, probabilities);
		render(picks, probabilities);
	 });
}

function render( picks, prob1){
			  
	renderTable(picks, prob1);
	
	data = picks.filter(function(d){return d[PROB_COLUMN] > 0}).map(function(d){ 
		d.value = d[PROB_COLUMN]; 
		return d; 
	});
    renderCircles(data);
}

function renderTable( picks, prob1){

						                   					
	var headerData = picks.header;	
	var picks_table = d3.select("#picks")
                    .append("table")
					.attr("class","table table-striped table-condensed");
	picks_table.append("thead").append("tr")
					.selectAll("th")
					.data(headerData)
					.enter().append("th")
					.text(function(d) {
						return d;
					});
	var sortedPicks = picks.sort(function(a, b) { 
		return b[PROB_COLUMN] - a[PROB_COLUMN]; 
	});
	
		var rows = picks_table.append("tbody").selectAll("tr")
            .data(sortedPicks).enter();
                    
			rows.append("tr")
             .selectAll("td")
             .data(function(d) { 
				return d;
			}).enter()
            .append("td")
            .text(function(d,i) { 
					
				if( i < PROB_COLUMN/2 +1 ){																		
					return d;								
				}
				if(  i!==PROB_COLUMN ){																		
					return parseInt(d);								
				}
				return Number(d).toFixed(2); 
			})
			.attr("class", function(d, i){
				var classname ="";
				if( isNaN(d, i)){
					var round = winnerRound(i);					
					classname = teamColor(d, round, prob1);					
					return classname;
				}else if ( i >= PROB_COLUMN/2 +1 && i < PROB_COLUMN ){
					var series = i - PROB_COLUMN/2 -1;					
					classname = gameCountColor(parseInt(d), series, prob1);	
					return classname;
				}									
			});

		var table = d3.select("#stats").append('table').attr("class","table table-striped table-condensed");
		var thead = table.append('thead')
		var	tbody_stats = table.append('tbody');
		var columns = ["team","win"];

		// append the header row
		thead.append('tr')
		  .selectAll('th')
		  .data(columns).enter()
		  .append('th')
		    .text(function (column) { 			
				return column;
			});
			
		// create a row for each object in the data
		var rows = tbody_stats.selectAll('tr')
		  .data(prob1.sort(function(a, b) { 
							return b.win[2] - a.win[2]; 
						}).filter(function(d){
			  return d.win[2] > 0;
		  }))
		  .enter()
		  .append('tr');

		// create a cell in each row for each column
		var cells = rows.selectAll('td')
		  .data(function (row) {
			  var ret = [];
			  ret.push( row.team);
			  row.win.forEach( function(w){
				  ret.push( row.team+","+w );
			  });
			  return ret; 
		  })
		  .enter()
		  .append('td')
		  .text(function (d, i) { 
			if( i > 0){	
		  		var odds = d.split(',')[1];
				return odds;

			}
			else{
			  return d;
			}
				
			})
			 .attr("class", function(d, i){
				var classname ="";
				var team = d.split(',')[0];
				if( i > 0){	
					classname = teamColor(team, i, prob1);					
					return classname;
				}								
			});;	  
}

function winnerRound(column){
	if( column == 4) return 4;
	return 3;
}

function renderCircles(data){
	//bubbles needs very specific format, convert data to this.
	var color = d3.scale.category10(); //color category
	var diameter = 300; //max size of the bubbles
	
	//add farq
	var farq = ["Farq"];
	farq.winProbability = 0;
	
	data.forEach(function(d){
		if( d[0].includes( "Marshal") || d[0].includes("Thurston")){
			d.winProbability /=2;
						d.value /=2;

			farq.winProbability += d.winProbability;
		}
	});
		farq.value = farq.winProbability;

	data.push(farq);
	var svg = d3.select("#chart")
    .append("svg")
    .attr("width", diameter)
    .attr("height", diameter)
    .attr("class", "bubble");
	
	var bubble = d3.layout.pack()
    .sort(null)
    .size([diameter, diameter])
    .padding(1.5);
	
    var nodes = bubble.nodes({children:data}).filter(function(d) { return !d.children; });

    //setup the chart
    var bubbles = svg.append("g")
        .attr("transform", "translate(0,0)")
        .selectAll(".bubble")
        .data(nodes.filter(function(d){
			return d.winProbability > 0.01;
		}))
        .enter();

    //create the bubbles
    bubbles.append("circle")
        .attr("r", function(d){ return d.r; })
        .attr("cx", function(d){ return d.x; })
        .attr("cy", function(d){ return d.y; })
        .style("fill", function(d) { 
			if(d[0] == "Jason")
				return JASON_COLOUR;
			return color(d[0]); 
		})
		.on("mouseover", function(d) {		
            div.transition()		
                .duration(200)		
                .style("opacity", .9);		
            
            })					
        .on("mouseout", function(d) {		
            div.transition()		
                .duration(200)		
                .style("opacity", 0);	
        });;

    //format the text for each bubble
    bubbles.append("text")
        .attr("x", function(d){ return d.x; })
        .attr("y", function(d){ return d.y + 5; })
        .attr("text-anchor", "middle")
        .html(function(d){ 
			var names = d[0].split("/");
			var html = "";
			var div = "";
			for( var n in names){
				html += div + names[n];
				div = "-";
			}
			return html; 
		})
		.on("mouseover", function(d) {		
            div.transition()		
                .duration(200)		
                .style("opacity", .9);		
         
            })					
        .on("mouseout", function(d) {		
            div.transition()		
                .duration(200)		
                .style("opacity", 0);	
        })
		.style( {
            "font-family":"Helvetica Neue, Helvetica, Arial, san-serif",
			"font-size":
			function(d) { 
				return Math.min(2 * d.r, (2 * d.r - 8) / this.getComputedTextLength() * 13) + "px"; 
		}});
}

function pointsSoFar(picks, prob1) {
	for( var j =0; j < picks.length; j++ ){		
		picks[j].gameGuess = gameGuessScore(picks[j]);
		var wins = scoreForWins( picks[j], prob1);
		picks[j].push( picks[j].gameGuess + wins);
	}	
}

function scoreForWins( choices, prob1){
	var score = 0;
	for( var i =0; i < choices.length; i++){
		var round  = roundNum(i);
		
		var choice = choices[i];
		for( var p = 0; p < prob1.length; p++ ){
			var prob = prob1[p];
			if( prob.team === choice){
				if( prob.win[round-1] === 1 ){
					score += pointsForRound( round );
				}
			}
		}		
	}
	return score;	
}

function calcProbabilityOfWin(picks, prob1) {
	
	var totalP = 0, count = 0;

	//for( var a =4; a <=7; a++){
	//	for( var b=4; b <=7; b++){
			for( var c =4; c <=7; c++){
			//	games[0].forEach(function (m) {
			//		games[1].forEach(function (n) {
						games[2].forEach(function (p) {
							if( p !== "VGK" && p != "WSH") return;
							var winners = [ "VGK", "WSH", p];
							var gameNums = [5,7,c];
							var prob = probOf(winners, gameNums, prob1);
							count++;
							totalP += prob;
							updateWinners(winners, gameNums, prob);
						});
					//});
				//});
			}
		//}
	//}
	console.log( count +" "+totalP);
}

function updateWinners(winners, games, prob){
	var poolWinnerList = findPoolWinner(winners, games);
//	console.log( prob +" "+poolWinnerList);
	for( var pi in poolWinnerList){
		var index = parseInt(poolWinnerList[pi]);		
		var pick = picks[index];		
		pick[PROB_COLUMN] += prob*100.0/poolWinnerList.length;
		pick.winProbability = picks[index][PROB_COLUMN];
	}
}



function gameGuessScoreProb( guesses, probabilities ){	
	var score = 1;
	for( var i =0; i < 3;i++){
		var guess = parseInt( guesses[i] );
		score *= singleGuessProb( guess, i, probabilities );
	}
	return score;
}

function singleGuessProb( guess, i, probabilities){
	var gscore = 0;
	var guessIndex = guess - 4;
	if( i == 2){
		guessIndex = guess;
	}
	if( i == 0){ 
		i = 1;
	}
	else if( i == 1){ 
		i = 0;
	}
	var pcount = 0;
	for( var j = 0; j < probabilities.length; j++ ){
		for( var g = 0; g < games[i].length; g++){
			if( probabilities[j].team == games[i][g] ) {
				var p = probabilities[j].gameNumChance[guessIndex];
				gscore +=p;
				if( p > 0 )
				  pcount++;
			}
		}	
	}		
	if( pcount == 0 ) return 0;
	return gscore/pcount;
}

function gameIndex( i ){
	if( i === 0 ) return 1;
	if( i === 1 ) return 0;
	return i;
}

function gameGuessScore( pick ){	
	var score = 0;
	for( var i =0; i < 3;i++){
		if( parseInt(pick[5+i])=== seriesGames[i])
			score++;
	}
	return score;
}

//should the current "number of games guesses" be included here?
function findPoolWinner(winners, games){
	var winnerList=[];
	var count = [];//track the "score" for each entry for this combination of winners
	for( var j =0; j < picks.length; j++ ){
		count[j] = 0;
		var pick = picks[j];
		for( var g = 0; g < 3; g++){
			if( games[g] === parseInt(pick[5+g]))
				count[j]++;
		}
	}
	//	console.log( count );

	for( var i =0; i < winners.length; i++){
		var pts = pointsForRound( roundNum(i) )
		for( var t=0;t< picks.length;t++ ){
			var pick = picks[t];
			var pickVal = pick[DATA_START+i];
			if( pickVal == winners[i]){
				count[t]+=pts;
			}
		}
	}
	var maxval = 0;
	for( var j=0;j< picks.length;j++ ){
		if( count[j] > maxval){
			maxval = count[j];
			winnerList=[];
		}
		if( count[j] == maxval){
			winnerList.push(j);
		}
	}
	return winnerList;
}

function pointsForRound( round ){
	var points = 2;
	if( round === 4 ) 
		points = 4;
	return points; 
}

//find the combined probability of the events in the winners array occurring
//this is wrong. It should calculate the probability of the combination of events but include the dependent
//odds
// the probability of a team reaching a later round is dependant on the probability of it making an earlier round
//for example, the odds of a team making the finals isn't the odds it will make each earlier round times the odds it will makes each //later round.
// it's just the odds of the final event
function probOf( winners, games, prob1){
	var  p = 1;
	var round;
	var done = [];// for conditional probability
	for( var i = winners.length-1; i >= 0; i--){
		if( !done.includes( winners[i]) ){
			p*= probability(winners[i], roundNumB(i), prob1);
			done.push(winners[i]);
		}
	}
	var gp =1;
	gp*= gameGuessScoreProb( games, prob1 );
	//console.log( games+" "+gp );
	p*=gp;
	return p;
}

function roundNumB( index ){
	var round = 2;
	if( index >= 2 ){
		round = 3 ;
	}		
	return round;
}

function roundNum( index ){
	var round = 3;
	if( index > 2 ){
		round = 4 ;
	}		
	return round;
}

function probability(team, round, prob1){
	for( var i in prob1){
		var prob = prob1[i];
		if(team == prob1[i].team){
			return prob1[i].win[round];
		}
	}
	console.log( team +" not found");
}
	
function teamColor( team, round, prob1 ){
	 for( var i in prob1){		 
		 if( team == prob1[i].team){
			 var w = prob1[i].win[round-1];
			 return probColour( w);
		 }
	 }
}

function gameCountColor( guess, series, probs){
	var w = singleGuessProb( guess, series, probs);
//	console.log( guess+" "+series+" "+w);
	return probColour( w);
}

function probColour( w ){
	if( w == 0 ) return "disabled";
	 else if( w == 1 ) return "won";
	 else if( w < 0.20 ) return "danger";
	 else if( w < 0.50 ) return "warning";
	 else if (w < .80)return "info";
	 else return "success";
	
}