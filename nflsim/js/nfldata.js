 nfldata = function() {
   var parseDate = d3.timeParse("%Y-%m-%d");
   var parseScheduleDate = d3.timeParse("%m/%d/%Y");

   let data = [];
   var callbackFcn;
   let filter_date = '2018-08-01';
   let week = 0;
   let avgN = 2;
   let totals = {}, defence={}


   function analyze(error, schedule, lines, elo, consensus, vegas) {
     schedule = inputSchedule(schedule);
     lines = inputLines(lines);
     elo = inputElo(elo);
     consensus = inputConsensus(consensus);
     vegas = inputVegas(vegas);
     data = linkData(schedule, lines, elo, consensus, vegas);
     data = data.sort(function(a, b){
       return a.gameDate - b.gameDate;
     });
     callbackFcn(data);
   }

   function inputVegas(vegas) {
     vegas = vegas.filter(function(d) {
       return d.Date.localeCompare(filter_date) > 0
     });
     vegas.forEach(function(v) {
       v.Date = parseDate(v.Date);
       v.openline = Number(v["Home Line Open"]);
       if( v["Home Line Close"] === ''){
          v.closeline = v.openline;
       }
       else{
         v.closeline = Number(v["Home Line Close"]);
       }
       v.scorediff = Number( v["Away Score"]-v["Home Score"] )

       if( !totals[v["Home Team"]] ){
         totals[v["Home Team"]] = 0;
       }
       if( !totals[v["Away Team"]] ){
         totals[v["Away Team"]] = 0;
       }
       v.homeAvg =  totals[v["Home Team"]];
       v.awayAvg = totals[v["Away Team"]];
       totals[v["Home Team"]] -= totals[v["Home Team"]]/3
       totals[v["Home Team"]] += Number(v["Home Score"])/3
       totals[v["Away Team"]] -=  totals[v["Away Team"]]/3
       totals[v["Away Team"]] +=  Number(v["Away Score"])/3

//
       if( !defence[v["Home Team"]] ){
         defence[v["Home Team"]] = 0;
       }
       if( !defence[v["Away Team"]] ){
         defence[v["Away Team"]] = 0;
       }
       v.homeDAvg =  defence[v["Home Team"]];
       v.awayDAvg = defence[v["Away Team"]];
       defence[v["Home Team"]] -= defence[v["Home Team"]]/ avgN
       defence[v["Home Team"]] += Number(v["Away Score"])/avgN
       defence[v["Away Team"]] -=  defence[v["Away Team"]]/avgN
       defence[v["Away Team"]] +=  Number(v["Home Score"])/avgN

     })
     return vegas;
   }

   function inputSchedule(data) {

     return data.gameSchedules.filter(function(d) {
       return d.seasonType === "REG" && (week === 0 || d.week == week);
     })

   }
   let nameFixes = [
     ["LAR", "LA"],
     ["WSH", "WAS"]
   ];

   function inputElo(data) {
     elo = data.filter(function(d) {
       return d.date.localeCompare(filter_date) > 0
     });
     elo.forEach(function(e) {
       e.date = parseDate(e.date);
       nameFixes.forEach(function(fix) {
         if (e.team1 === fix[0]) e.team1 = fix[1]
         if (e.team2 === fix[0]) e.team2 = fix[1]
       })
     })
     return elo;
   }

   function inputLines(data) {
     var rows = data.split("\n").filter(function(r) {
       return r.length > 10;
     });
     return rows.map(function(r) {
       var regEx = /([A-Z a-z]+)([0-9.]+)([A-Z a-z]+)/ig;
       var match = regEx.exec(r);
       return {
         favorite: match[1].trim(),
         line: Number(match[2]),
         underdog: match[3].trim()
       };
     })
   }

   function inputConsensus(data) {
     var days = data.split("#TIME/TVNFL FOOTBALL").filter(function(r) {
       return r.length > 10;
     }).slice(1, 4);

     consensus = [];
     days.forEach(function(day) {
       let gameList = day.split(":").slice(1);
       gameList.forEach(function(g) {
         let fields = g.split("\n");
         if (fields.length > 4) {
           let visitorLine = parseLine(fields[3]);
           let homeLine = parseLine(fields[4]);
           consensus.push({
             visitor: fields[1].replace(/[^a-zA-Z ]/g, ""),
             home: fields[2].replace(/[^a-zA-Z ]/g, ""),
             visitorLine: visitorLine[0],
             homeLine: homeLine[0],
             visitorOdds: visitorLine[1],
             homeOdds: homeLine[1]
           });
         }
       })
     })
     return consensus;
   }

   function linkData(schedule, lines, elo, consenus, vegas) {
     let teams = {}

     schedule.forEach(function(game) {
       game.gameDate = parseScheduleDate(game.gameDate);
       if (!teams[game.homeDisplayName]) {
         teams[game.homeDisplayName] = game.homeTeam;
       }
       if (!teams[game.visitorDisplayName]) {
         teams[game.visitorDisplayName] = game.visitorTeam;
       }
     });

       vegas.forEach(function(vega){
          let found = false;
          schedule.forEach(function(game) {
            if( vega["Home Team"] == game.homeDisplayName && vega["Away Team"] == game.visitorDisplayName)
              if( vega.Date.getTime() == game.gameDate.getTime()){
                game.vegas = vega;
                game.homeAvg = vega.homeAvg;
                game.homeDAvg = vega.homeDAvg;
                game.awayAvg = vega.awayAvg;
                game.awayDAvg = vega.awayDAvg;
                game.line = vega.openline;
                game.closeline = vega.closeline;
                game.lineGap = Math.abs( Number(game.vegas["Home Line Close"]) - Number(game.vegas["Home Line Open"]))
                found = true;
             }
         })
         if( !found ){
           let home = vega["Home Team"]
           let away = vega["Away Team"]
           let hteam = teams[home];
           let vteam = teams[away];
           if( hteam != null && vteam != null){
              schedule.push(
                {homeTeam:teams[home],
                  visitorTeam:teams[away],
                  gameDate:vega.Date,
                  vegas:vega,
                  line:vega.openline,
                  homeAvg:vega.homeAvg,
                  homeDAvg:vega.homeDAvg,
                  awayAvg:vega.awayAvg,
                  awayDAvg :vega.awayDAvg,
              homeAvg:totals[teams[home].fullName],
              awayAvg:totals[teams[away].fullName],
               week:8})
           }
         }
       })

     schedule.forEach(function(game) {
       if( game.gameDate.getFullYear() === 2006){
         let debug = 0;
       }
       game.week = (game.gameDate.getMonth()-8)
       elo.forEach(function(e) {
         if (game.homeTeam.abbr === e.team1 && game.visitorTeam.abbr === e.team2) {
           if( game.gameDate.getTime() === e.date.getTime()){
             game.elo = (e.elo2_pre - e.elo1_pre) / 25;
             game.elo -= 65 / 25;
             game.elo = -game.elo;
           }
         }
       })


       if (!game.vegas) {
         game.homeAvg = totals[game.homeDisplayName]
         game.homeDAvg = defence[game.homeDisplayName]
         game.awayAvg = totals[game.visitorDisplayName]
         game.awayDAvg = defence[game.visitorDisplayName]

         lines.forEach(function(line) {
           if (match(game, line, true)) {
             game.line = line.line;
             game.lineFavorite = line.favorite;
             game.lineUnderdog = line.underdog;
             game.tomline = line;
           }
           if (match(game, line, false)) {
             game.line = -line.line;
             game.lineFavorite = line.favorite;
             game.lineUnderdog = line.underdog;
             game.tomline = line;
           }
         })

         consensus.forEach(function(con) {
           if (matchCon(game, con, true)) {
             game.consensus = con;
             game.conFavorite = con.homeLine < con.visitorLine ? con.home : con.visitor;
             game.conOdds = con.homeOdds;
             game.conLine = -con.homeLine;
           }
         })
         if( game.consensus)
         game.lineGap = Math.abs( game.conLine - game.line);
       //   console.log(game);
       }
     })
     return schedule;
   }

   function match(game, line, inv) {
     let homeCity = game.homeTeam.cityState.toLowerCase();
     let visitorCity = game.visitorTeam.cityState.toLowerCase()
     let homeNick = game.homeTeam.nick.toLowerCase()
     let visitorNick = game.visitorTeam.nick.toLowerCase();
     let favorite = inv ? line.favorite.toLowerCase() : line.underdog.toLowerCase();
     let underdog = inv ? line.underdog.toLowerCase() : line.favorite.toLowerCase();
     if (homeCity === favorite || favorite.indexOf(homeNick) >= 0) {
       if (visitorCity === underdog || underdog.indexOf(visitorNick) >= 0)
         return true;
     }
     return false;
   }

   function matchCon(game, con, inv) {
     let homeCity = game.homeTeam.cityState.toLowerCase().trim();
     let visitorCity = game.visitorTeam.cityState.toLowerCase().trim()
     let homeNick = game.homeTeam.nick.toLowerCase().trim()
     let visitorNick = game.visitorTeam.nick.toLowerCase().trim();
     let favorite = inv ? con.home.toLowerCase() : con.visitor.toLowerCase();
     let underdog = inv ? con.visitor.toLowerCase() : con.home.toLowerCase();
     let a = homeCity.localeCompare(favorite);
     let b = visitorCity.localeCompare(underdog);
     if (homeCity.localeCompare(favorite) == 0 || favorite.indexOf(homeNick) >= 0) {
       if (visitorCity.localeCompare(underdog) == 0 || underdog.indexOf(visitorNick) >= 0)
         return true;
     }
     return false;
   }

   function parseLine(field) {
     var val;
     field = field.replace("PK", "PK ");
     string = field.split(" ")[0]
     if (string.indexOf("PK") >= 0) {
       val = 0;
     } else {
       val = Number(string);
       if (isNaN(val)) {
         val = Number(string.substring(0, string.length - 1))
         if (val < 0) {
           val -= 0.5;
         } else {
           val += 0.5;
         }
       }
     }
     let odds = Number(field.split(" ")[1]);
     return [val, odds];
   }

   function schedule() {

     return schedule;
   }
   schedule.setFilterDate = function(date) {
     filter_date = date;
     return schedule;
   }
   schedule.setWeek = function(aweek) {
     week = aweek;
     return schedule;
   }

   schedule.getData = function(callback) {
     callbackFcn = callback;
     d3.queue()
       .defer(d3.json, "./data/schedules.json")
       .defer(d3.text, "./data/lines"+week+".txt")
       .defer(d3.csv, "./data/nfl_elo.csv")
       .defer(d3.text, "./data/consensus.txt")
       .defer(d3.csv, "./data/nfl.csv")

       .await(analyze);

     return data;
   }

   return schedule;
 }
