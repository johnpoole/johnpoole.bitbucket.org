  posPercent = 34.25;
  prevalence = 0;
  sensitivity = 77;
  specificity = 98;
  numberOfTests = 100;
  ppv = 0;
  npv = 0;
function bubbleChart() {
  // Constants for sizing
  var width = 600;
  var height = 350;
 
  // tooltip for mouseover functionality
  var tooltip = floatingTooltip('gates_tooltip', 240);

  // Locations to move bubbles towards, depending
  // on which view mode is selected.
  var center = { x: width / 2, y: height * 3/ 5};

  var resultCenters = {
   Positive: { x: width / 3, y: height * 3 / 5 },
    Negative: { x: 2 * width / 3, y: height * 3 / 5 }
  };

  // X locations of the year titles.
  var resultsTitleX = {
    Positive: 160,
    Negative: width - 160
  };

  // @v4 strength to apply to the position forces
  var forceStrength = 0.03;

  // These will be set in create_nodes and create_vis
  var svg = null;
  var bubbles = null;
  var nodes = [];

  // Charge function that is called for each node.
  // As part of the ManyBody force.
  // This is what creates the repulsion between nodes.
  //
  // Charge is proportional to the diameter of the
  // circle (which is stored in the radius attribute
  // of the circle's associated data.
  //
  // This is done to allow for accurate collision
  // detection with nodes of different sizes.
  //
  // Charge is negative because we want nodes to repel.
  // @v4 Before the charge was a stand-alone attribute
  //  of the force layout. Now we can use it as a separate force!
  function charge(d) {
    return -Math.pow(d.radius, 2.0) * forceStrength;
  }

  // Here we create a force layout and
  // @v4 We create a force simulation now and
  //  add forces to it.
  var simulation = d3.forceSimulation()
    .velocityDecay(0.2)
    .force('x', d3.forceX().strength(forceStrength).x(center.x))
    .force('y', d3.forceY().strength(forceStrength).y(center.y))
    .force('charge', d3.forceManyBody().strength(charge))
    .on('tick', ticked);

  // @v4 Force starts up automatically,
  //  which we don't want as there aren't any nodes yet.
  simulation.stop();

  // Nice looking colors - no reason to buck the trend
  // @v4 scales now have a flattened naming scheme
  var fillColor = d3.scaleOrdinal()
    .domain(['sick', 'healthy'])
    .range(['#d84b2a', '#7aa25c']);


  /*
   * This data manipulation function takes the raw data from
   * the CSV file and converts it into an array of node objects.
   * Each node will store data and visualization values to visualize
   * a bubble.
   *
   * rawData is expected to be an array of data objects, read in from
   * one of d3's loading functions like d3.csv.
   *
   * This function returns the new node array, with a node in that
   * array for each element in the rawData input.
   */
  function createNodes(rawData) {
    // Use the max total_amount in the data as the max in the scale's domain
    // note we have to ensure the total_amount is a number.
    var maxAmount = d3.max(rawData, function (d) { return +d.total_amount; });

    // Sizes bubbles based on area.
    // @v4: new flattened scale names.
    var radiusScale = d3.scalePow()
      .exponent(0.5)
      .range([2, 5])
      .domain([0, maxAmount]);

    // Use map() to convert raw data into node data.
    // Checkout http://learnjsdata.com/ for more on
    // working with data.
    var myNodes = rawData.map(function (d) {
      return {
        id: d.id,
        radius: radiusScale(+d.total_amount),
        value: +d.total_amount,
        name: d.grant_title,
        org: d.organization,
        group: d.group,
        result: d.result,
        x: Math.random() * 900,
        y: Math.random() * 800
      };
    });

    // sort them to prevent occlusion of smaller nodes.
    myNodes.sort(function (a, b) { return b.value - a.value; });

    return myNodes;
  }

  /*
   * Main entry point to the bubble chart. This function is returned
   * by the parent closure. It prepares the rawData for visualization
   * and adds an svg element to the provided selector and starts the
   * visualization creation process.
   *
   * selector is expected to be a DOM element or CSS selector that
   * points to the parent element of the bubble chart. Inside this
   * element, the code will add the SVG continer for the visualization.
   *
   * rawData is expected to be an array of data objects as provided by
   * a d3 loading function like d3.csv.
   */
  var chart = function chart(selector, rawData) {
    // convert raw data into nodes data
    nodes = createNodes(rawData);
	svg = d3.select(selector).select("svg").remove();
    // Create a SVG element inside the provided selector
    // with desired size.
    svg = d3.select(selector)
      .append('svg')
      .attr('width', width)
      .attr('height', height);

    // Bind nodes data to what will become DOM elements to represent them.
    bubbles = svg.selectAll('.bubble')
      .data(nodes, function (d) { return d.id; });

    // Create new circle elements each with class `bubble`.
    // There will be one circle.bubble for each object in the nodes array.
    // Initially, their radius (r attribute) will be 0.
    // @v4 Selections are immutable, so lets capture the
    //  enter selection to apply our transtition to below.
    var bubblesE = bubbles.enter().append('circle')
      .classed('bubble', true)
      .attr('r', 0)
      .attr('fill', function (d) { return fillColor(d.group); })
      .attr('stroke', function (d) { return d3.rgb(fillColor(d.group)).darker(); })
      .attr('stroke-width', 0)
      .on('mouseover', showDetail)
      .on('mouseout', hideDetail);

    // @v4 Merge the original empty selection and the enter selection
    bubbles = bubbles.merge(bubblesE);

    // Fancy transition to make bubbles appear, ending with the
    // correct radius
    bubbles.transition()
      .duration(2000)
      .attr('r', function (d) { return d.radius; });

    // Set the simulation's nodes to our newly created nodes array.
    // @v4 Once we set the nodes, the simulation will start running automatically!
    simulation.nodes(nodes);

    // Set initial layout to single group.
    groupBubbles();
  };

  /*
   * Callback function that is called after every tick of the
   * force simulation.
   * Here we do the acutal repositioning of the SVG circles
   * based on the current x and y values of their bound node data.
   * These x and y values are modified by the force simulation.
   */
  function ticked() {
    bubbles
      .attr('cx', function (d) { return d.x; })
      .attr('cy', function (d) { return d.y; });
  }

  /*
   * Provides a x value for each node to be used with the split by year
   * x force.
   */
  function nodeResultPos(d) {
    return resultCenters[d.result].x;
  }


  /*
   * Sets visualization in "single group mode".
   * The year labels are hidden and the force layout
   * tick function is set to move all nodes to the
   * center of the visualization.
   */
  function groupBubbles() {
    hideResultTitles();

    // @v4 Reset the 'x' force to draw the bubbles to the center.
    simulation.force('x', d3.forceX().strength(forceStrength).x(center.x));

    // @v4 We can reset the alpha value and restart the simulation
    simulation.alpha(1).restart();
  }


  /*
   * Sets visualization in "split by year mode".
   * The year labels are shown and the force layout
   * tick function is set to move nodes to the
   * yearCenter of their data's year.
   */
  function splitBubbles() {
    showResultTitles();

    // @v4 Reset the 'x' force to draw the bubbles to their year centers
    simulation.force('x', d3.forceX().strength(forceStrength).x(nodeResultPos));

    // @v4 We can reset the alpha value and restart the simulation
    simulation.alpha(1).restart();
  }

  /*
   * Hides Result title displays.
   */
  function hideResultTitles() {
    svg.selectAll('.result').remove();    
	svg.selectAll('.ppv').remove();
  }

  /*
   * Shows Result title displays.
   */
  function showResultTitles() {
    // Another way to do this would be to create
    // the result texts once and then just hide them.
    var resultsData = d3.keys(resultsTitleX);
    var results = svg.selectAll('.result')
      .data(resultsData);

    results.enter().append('text')
      .attr('class', 'result')
      .attr('x', function (d) { return resultsTitleX[d]; })
      .attr('y', 15)
      .attr('text-anchor', 'middle')
      .text(function (d) { return  "Tested "+ d; });
	  
	  var pvx = {
		PPV: { x :160, val: ppv},
		NPV: { x: width - 160, val: npv}
	  };
	  
	 var pvxDisplay = svg.selectAll('.ppv')
      .data(Object.entries(pvx));

    pvxDisplay.enter().append('text')
      .attr('class', 'ppv')
      .attr('x', function (d) { return d[1].x })
      .attr('y', 30)
      .attr('text-anchor', 'middle')
      .text(function (d) { 
		var val = +d[1].val;
		return d[0]+' = ' + val.toFixed(4);
	});
  }


  /*
   * Function called on mouseover to display the
   * details of a bubble in the tooltip.
   */
  function showDetail(d) {
    // change outline to indicate hover state.
    d3.select(this).attr('stroke', 'black');

    var content = '<span class="name">Actual: </span><span class="value">' +
                  d.group +
                  '</span><br/>' +                 
                  '<span class="name">Result: </span><span class="value">' +
                  d.result +
                  '</span>';

    tooltip.showTooltip(content, d3.event);
  }

  /*
   * Hides tooltip
   */
  function hideDetail(d) {
    // reset outline
    d3.select(this)
      .attr('stroke', d3.rgb(fillColor(d.group)).darker());

    tooltip.hideTooltip();
  }

  /*
   * Externally accessible function (this is attached to the
   * returned chart function). Allows the visualization to toggle
   * between "single group" and "split by year" modes.
   *
   * displayName is expected to be a string and either 'year' or 'all'.
   */
  chart.toggleDisplay = function (displayName) {
    if (displayName === 'result') {
      splitBubbles();
    } else {
      groupBubbles();
    }
  };


  // return the chart function from closure.
  return chart;
}

/*
 * Below is the initialization code as well as some helper functions
 * to create a new bubble chart instance, load the data, and display it.
 */

var myBubbleChart = bubbleChart();

/*
 * Function called once data is loaded from CSV.
 * Calls bubble chart function to display inside #vis div.
 */
function display(error, data) {
  if (error) {
    console.log(error);
  }

  myBubbleChart('#vis', data);
}

/*
 * Sets up the layout buttons to allow for toggling between view modes.
 */
function setupButtons() {
  var toolbar = d3.select('#toolbar');
    toolbar.selectAll('.btn')
    .on('click', function () {
      // Remove active class from all buttons
      d3.selectAll('.btn').classed('active', false);
      // Find the button just clicked
      var button = d3.select(this);

      // Set it as the active button
      button.classed('active', true);

      // Get the id of the button
      var buttonId = button.attr('id');

      // Toggle the bubble chart based on
      // the currently clicked button.
      myBubbleChart.toggleDisplay(buttonId);
    });
	
	d3.selectAll('#posPercent').attr("value", posPercent)
	.on("change", function() {
		posPercent = +this.value;
		generateData();
	});
	//Sensitivity measures the proportion of positives that are correctly identified (e.g., the percentage of sick people who are correctly identified as having some illness).
	
	d3.selectAll('#sens').attr("value", sensitivity)
	.on("change", function() {
		sensitivity = +this.value;
		generateData();
	});
	
	d3.selectAll('#spec').attr("value", specificity)
	.on("change", function() {
		specificity = +this.value;
		generateData();
	});
}


// Load the data.
//d3.csv('data/gates_money.csv', display);
function generateData(){
	var data = [];
	prevalence = calcPrevalence( posPercent, specificity, sensitivity);
	d3.select('#prev').attr("value", prevalence.toFixed(6));
	 
	for( var i = 0; i < numberOfTests; i++){
		var group = Math.floor(Math.random() * 1000) < prevalence*10 ? "sick" : "healthy";
		var result ;
		
		if( group === 'sick')
			result = Math.floor(Math.random() * 100) < sensitivity ? "Positive": "Negative" ;
		else
			result = Math.floor(Math.random() * 100) < specificity ?  "Negative" : "Positive";

		var amount = 3;
		data.push( {grant_title:"test"+i,id:i,organization:"eee", total_amount:amount, group:group, result});
	}
	var ppvNum  =  data.filter( d => d.result == "Positive" && d.group == "sick" ).length;
	var ppvDenom = data.filter( d => d.result == "Positive").length
	var   npvNum =  data.filter( d => d.result == "Negative" && d.group == "healthy" ).length;
	var   npvDenom = data.filter( d => d.result == "Negative").length

	ppv = ppvNum / ppvDenom;
	npv =  npvNum / npvDenom;

	d3.select("#all").classed('active', true);
	d3.select("#result").classed('active', false);

	display(null,data);	
}

function calcPrevalence( pos, spec, sens ){
    var sPb = 1 - spec/100;
	return ( pos/100 - sPb ) / (sens/100 - sPb)*100;
}

generateData();

// setup the buttons.
setupButtons();
